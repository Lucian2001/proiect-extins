package socialnetwork.ui;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.exceptions.BadDateException;
import socialnetwork.service.*;
import socialnetwork.service.exceptions.ServiceException;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;

public class Ui {
    UtilizatorService userService;
    PrietenieService friendshipService;
    ComunityService comunityService;
    FriendRequestService friendRequestService;
    MessageService messageService;
    GroupsService groupsService;

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public Ui(UtilizatorService userService, PrietenieService friendshipService, ComunityService comunityService, FriendRequestService friendRequestService, MessageService messageService, GroupsService groupsService) {
        this.userService = userService;
        this.friendshipService = friendshipService;
        this.comunityService = comunityService;
        this.friendRequestService = friendRequestService;
        this.messageService = messageService;
        this.groupsService = groupsService;
    }


    public void run() throws IOException {
        Integer cmd = -1;
        while(cmd != 0) {
            System.out.println("Alege o optiune de la 1 la 8:");
            System.out.println("1 - Add utilizator:");
            System.out.println("3 - Remove utilizator");
            System.out.println("4 - Remove prietenie");
            System.out.println("5 - Afisare numar de comunitati");
            System.out.println("6 - Afisare cea mai sociabila comunitate");
            System.out.println("7 - Afiseaza utilizatori");
            System.out.println("8 - Afiseaza prietenii");
            System.out.println("9 - Afiseaza prietenii unui user");
            System.out.println("10 - Afiseaza prietenii dintr-o anumita luna");
            System.out.println("11 - Schimba utilizatorul");
            System.out.println("12 - Create group");
            System.out.println("13 - Show groups");
            System.out.println("14 - Show conversatie");
            System.out.println("0 - Exit!");
            try {
                cmd = Integer.parseInt(reader.readLine());
            } catch (NumberFormatException ex){
                //System.out.println(ex.toString());
            }
            try {


                switch (cmd) {
                    case 1:
                        uiAddUser();
                        break;
                    case 2:
                        //uiAddPrietenie();
                        break;
                    case 3:
                        uiRemoveUser();
                        break;
                    case 4:
                        //uiRemoveFriendship();
                        break;
                    case 5:
                        uiGetNumberOfComunities();
                        break;
                    case 6:
                        uiGetMostSociableComunity();
                        break;
                    case 7:
                        uiShowUsers();
                        break;
                    case 8:
                        uiShowFriendships();
                        break;
                    case 9:
                        uiUserFriends();
                        break;
                    case 10:
                        uiUserFriendsOnSpecificMonth();
                        break;
                    case 11:
                        uiSelectUser();
                        break;
                    case 12:
                        uiCreateGroup();
                        break;
                    case 13:
                        uiShowGroups();
                        break;
                    case 14:
                        uiShowConversation();
                    case 0:
                        break;
                    default:
                        System.out.println("Alege o comanda valida!");
                        break;
                }
            }catch(IOException ex){
                System.out.println("Introduceti valorile corespunzator!");
            }catch(ValidationException ex){
                System.out.println(ex.getMessage());
            } catch (ServiceException ex){
                System.out.println(ex.getMessage());
            } catch (IllegalArgumentException ex){
                System.out.println("Something nasty happened!");
            } catch (NullPointerException ex){
                ex.printStackTrace();
            } catch (BadDateException ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    void uiAddUser() throws IOException, ValidationException {
        String name, lastName;
        System.out.print("Introdu prenume: ");
        name = reader.readLine();
        System.out.print("Introdu nume de familie: ");
        lastName = reader.readLine();
        //userService.addUser(name, lastName);
        System.out.println("Comanda s-a efectuat cu succes!");
    }

    void uiAddPrietenie() throws IOException, ValidationException{
        Long id1, id2;
        System.out.print("Introdu id user: ");
        id1 = Long.parseLong(reader.readLine());
        System.out.print("Introdu id user: ");
        id2 = Long.parseLong(reader.readLine());
        friendshipService.addFriendship(id1, id2);
        System.out.println("Comanda s-a efectuat cu succes!");
    }

    void uiRemoveUser() throws IOException, ValidationException{
        Long id;
        System.out.print("Introdu id user: ");
        id  = Long.parseLong(reader.readLine());
        userService.removeUser(id);
        System.out.println("Comanda s-a efectuat cu succes!");
    }

    void uiRemoveFriendship() throws IOException, ValidationException{
        Long id1, id2;
        System.out.print("Introdu id user1: ");
        id1 = Long.parseLong(reader.readLine());
        System.out.print("Introdu id user2: ");
        id2 = Long.parseLong(reader.readLine());
        friendshipService.removeFriendship(id1, id2);
        System.out.println("Comanda s-a efectuat cu succes!");
    }

    void uiGetNumberOfComunities() throws IOException, ValidationException{
        System.out.println(comunityService.getNumberOfComunities());
    }

    void uiGetMostSociableComunity() throws IOException, ValidationException{
        ArrayList<Utilizator> users  = comunityService.getMostSociableComunity();
        for(Utilizator user: users){
            System.out.println(user.getId().toString() + " " + user);
        }
    }

    void uiShowUsers(){
        Iterable<Utilizator> users = userService.getAll();
        users.forEach(user -> System.out.println(user.getId().toString() + " " + user));
    }


    void uiShowFriendships(){
        Iterable<Prietenie> friendships = friendshipService.getAll();
        friendships.forEach(friendship -> System.out.println(friendship.getId().toString() + " " + friendship));
    }



    void uiUserFriends() throws IOException, ServiceException{
        Long id;
        System.out.print("Introdu id user: ");
        id = Long.parseLong(reader.readLine());
      //  List<FriendDTO> friends = friendshipService.getUserFriends(id);
      //  friends.forEach(System.out::println);
      //  if(friends.size() == 0){
    //        System.out.println("Utilizatorul nu are niciun prieten!");
    //    }
    }

    void uiUserFriendsOnSpecificMonth() throws IOException, ServiceException,BadDateException {
        Long id;
        Integer month, year;
        System.out.print("Introdu id user: ");
        id = Long.parseLong(reader.readLine());
        System.out.print("Introdu luna: ");
        month = Integer.parseInt(reader.readLine());
        System.out.print("Introdu anul: ");
        year = Integer.parseInt(reader.readLine());
        if(year < 0 || year > LocalDateTime.now().getYear()){
            throw new BadDateException("Anul nu este valid!");
        }
        if(month > 12 || month < 1 ){
            throw new BadDateException("Luna nu este valida!");
        }
        if(year == LocalDateTime.now().getYear() && month > LocalDateTime.now().getMonthValue()){
            throw new BadDateException("Luna nu este valida!");
        }

        List<FriendDTO> friends = friendshipService.getUserFriendsOnSpecificDate(id, month, year);
        friends.forEach(System.out::println);
        if(friends.size() == 0){
            System.out.println("Utilizatorul nu are niciun prieten!");
        }
    }

    void uiSelectUser() throws IOException, ServiceException{

        System.out.print("Introdu id user: ");
        Long id = Long.parseLong(reader.readLine());
        if(userService.userExist(id)){
            UserMenu userMenu = new UserMenu(id,friendRequestService, messageService);
            userMenu.run();
        } else{
            System.out.println("Nu exista acest utilizator!");
        }
    }

    void uiCreateGroup() throws IOException {
        System.out.println("Introdu id-urile user-urilor carora vrei sa le trimiti mesaj: ");
        System.out.println("Introdu un 0 cand vrei sa te opresti: ");
        Long id;
        List<Long> userIdList = new ArrayList<Long>();
        id = Long.parseLong(reader.readLine());
        if (id == 0){
            System.out.println("Introdu un user valid!");
        }
        do{
            userIdList.add(id);
            id = Long.parseLong(reader.readLine());
        } while (id != 0);
        groupsService.createGroup(userIdList);
    }

    void uiShowGroups(){
        for(Group group:groupsService.getAll()){
            System.out.println(group);
        }
    }

    void uiShowConversation() throws IOException {
        System.out.print("Introdu id grup: ");
        Long id = Long.parseLong(reader.readLine());
        ConversationDTO conversatie = groupsService.getConversation(id);
        System.out.println(conversatie);
    }


}
