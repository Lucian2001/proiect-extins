package socialnetwork.ui;

import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.exceptions.BadDateException;
import socialnetwork.service.FriendRequestService;
import socialnetwork.service.MessageService;
import socialnetwork.service.exceptions.ServiceException;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class UserMenu {
    Long idUser;
    FriendRequestService friendRequestService;
    MessageService messageService;

    public UserMenu(Long idUser, FriendRequestService friendRequestService, MessageService messageService) {
        this.idUser = idUser;
        this.friendRequestService = friendRequestService;
        this.messageService = messageService;
    }

    static BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
    void run(){
        Integer cmd = -1;
        while(cmd != 0) {
            System.out.println("Esti userul " + idUser);
            System.out.println("Alege o optiune de la 1 la 8:");
            System.out.println("1 - Add prieten:");
            System.out.println("2 - Afiseaza cererile de prietenie primite");
            System.out.println("3 - Rezolva cerere de prietenie");
            System.out.println("4 - Trimite mesaj");
            System.out.println("0 - Exit!");
            try {
                cmd = Integer.parseInt(reader.readLine());
            } catch (NumberFormatException ex){
                //System.out.println(ex.toString());
            } catch (IOException ex){

            }
            try {


                switch (cmd) {
                    case 1:
                        uiAddFriend();
                        break;
                    case 2:
                        uiShowFriendRequests();
                        break;
                    case 3:
                        uiSolveFriendRequests();
                        break;
                    case 4:
                        uiSendNewMessage();
                    case 0:
                        break;
                    default:
                        System.out.println("Alege o comanda valida!");
                        break;
                }
            }catch(IOException ex){
                System.out.println("Introduceti valorile corespunzator!");
            }catch(ValidationException ex){
                System.out.println(ex.getMessage());
            } catch (ServiceException ex){
                System.out.println(ex.getMessage());
            } catch (IllegalArgumentException ex){
                System.out.println("Something nasty happened!");
            } catch (NullPointerException ex){
                ex.printStackTrace();
            } catch (BadDateException ex){
                System.out.println(ex.getMessage());
            }
        }
    }

    void uiAddFriend() throws IOException{
        System.out.print("Introdu id user: ");
        Long id = Long.parseLong(reader.readLine());
        friendRequestService.addFriendRequest(idUser, id);
        System.out.println("Operatia s-a executat cu succes!");
    }

    void uiShowFriendRequests(){
        friendRequestService.getAllFriendRequests(idUser).forEach(System.out::println);
    }

    void uiSolveFriendRequests() throws IOException, ServiceException{
        System.out.print("Introdu id-ul user-ului: ");
        Long id = Long.parseLong(reader.readLine());
        System.out.println("Daca doresti sa accepti prietenia apasa 1, altfel apasa 0!");
        Integer accepted = Integer.parseInt(reader.readLine());
        if(accepted != 0 && accepted != 1){
            System.out.println("Nu ai introdus datele corect!");
        }
        friendRequestService.solveFriendRequest(idUser, id, accepted);
        System.out.println("Operatia s-a executat cu succes!");
    }

    void uiSendNewMessage() throws IOException {
        System.out.print("Introdu grupul in care vrei sa trimiti mesajul:");
        Long id = Long.parseLong(reader.readLine());
        System.out.print("Introdu mesajul dorit:");
        String message = reader.readLine();
        if(message.isEmpty()){
            System.out.print("Introdu un mesaj valid!");
        }
        messageService.sendMessage(idUser, id, message);
    }
}
