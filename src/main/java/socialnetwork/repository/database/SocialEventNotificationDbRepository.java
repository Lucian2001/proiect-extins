package socialnetwork.repository.database;

import socialnetwork.domain.SocialEventNotification;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

public class SocialEventNotificationDbRepository extends AbstractDbRepository<Long, SocialEventNotification> {

    public SocialEventNotificationDbRepository(String url, String username, String password, Validator<SocialEventNotification> validator) {
        super(url, username, password, validator);
    }

    @Override
    protected void loadData() {
        try{
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM events_notifications");
            while ( rs.next() ) {
                Long id = rs.getLong("id");
                Long event_id = rs.getLong("event_id");
                Long user_id = rs.getLong("user_id");
                Statement stmtEvent = c.createStatement();
                String select = "SELECT * FROM events WHERE id = ?";
                PreparedStatement ps = c.prepareStatement(select);
                ps.setLong(1, event_id);
                ResultSet rsEvent = ps.executeQuery();
                rsEvent.next();
                String eventName = rsEvent.getString("name");
                LocalDateTime date = rsEvent.getTimestamp("date").toLocalDateTime();
                SocialEventNotification socialEventNotification = new SocialEventNotification(date, user_id, event_id, eventName);
                socialEventNotification.setId(id);
                entities.put(socialEventNotification.getId(), socialEventNotification);
            }

            rs.close();
            stmt.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    ResultSet savePreparedStatement(SocialEventNotification entity) throws SQLException {
        String insert = "INSERT INTO events_notifications(event_id, user_id)  VALUES (?, ?) RETURNING id";
        PreparedStatement ps = c.prepareStatement(insert);
        ps.setLong(1, entity.getSocialEvent());
        ps.setLong(2, entity.getUser());

        return ps.executeQuery();
    }

    @Override
    Long getIdFromDb(ResultSet x) {
        try {
            x.next();
            Long id = x.getLong(1);
            return id;
        } catch (SQLException e){

        }
        return null;
    }

    @Override
    PreparedStatement deletePreparedStatement(Long id) throws SQLException {
        String delete = "DELETE FROM events_notifications WHERE ID = ?";
        PreparedStatement ps = c.prepareStatement(delete);
        ps.setInt(1, id.intValue());
        return ps;
    }

    @Override
    PreparedStatement updatePreparedStatement(SocialEventNotification entity) throws SQLException {
        return null;
    }
}
