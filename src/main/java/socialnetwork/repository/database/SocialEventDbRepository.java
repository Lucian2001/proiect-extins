package socialnetwork.repository.database;

import socialnetwork.domain.SocialEvent;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;

public class SocialEventDbRepository extends AbstractDbRepository<Long, SocialEvent> {
    public SocialEventDbRepository(String url, String username, String password, Validator<SocialEvent> validator) {
        super(url, username, password, validator);
    }

    @Override
    protected void loadData() {
        try{
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM events");
            while ( rs.next() ) {
                Long id = rs.getLong("id");
                String  name = rs.getString("name");
                String  description = rs.getString("description");
                Long group_id = rs.getLong("group_id");
                Long creator = rs.getLong("creator");
                String image = rs.getString("image");
                LocalDateTime date = rs.getTimestamp("date").toLocalDateTime();
                SocialEvent event  = new SocialEvent(name, image,description, creator, group_id, date);
                event.setId(id);
                entities.put(event.getId(), event);
            }

            rs.close();
            stmt.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    ResultSet savePreparedStatement(SocialEvent entity) throws SQLException {
        String insert = "INSERT INTO events(name, description, group_id, image, creator, date)  VALUES (?, ?, ?, ?, ?, ?) RETURNING id";
        PreparedStatement ps = c.prepareStatement(insert);
        ps.setString(1, entity.getName());
        ps.setString(2, entity.getDescription());
        ps.setLong(3, entity.getGroup());
        ps.setString(4, entity.getImage());
        ps.setLong(5, entity.getCreator());
        ps.setTimestamp(6, Timestamp.valueOf(entity.getDate()));
        return ps.executeQuery();
    }

    @Override
    Long getIdFromDb(ResultSet x) {
        try {
            x.next();
            Long id = x.getLong(1);
            return id;
        } catch (SQLException e){

        }
        return null;
    }

    @Override
    PreparedStatement deletePreparedStatement(Long aLong) throws SQLException {
        return null;
    }

    @Override
    PreparedStatement updatePreparedStatement(SocialEvent entity) throws SQLException {
        return null;
    }
}
