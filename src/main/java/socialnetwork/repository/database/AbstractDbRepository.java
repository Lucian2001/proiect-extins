package socialnetwork.repository.database;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.memory.InMemoryRepository;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

public abstract class AbstractDbRepository<ID, E extends Entity<ID>> extends InMemoryRepository<ID,E>{
    private String url;
    private String username;
    private String password;

    protected Connection c = null;
    public AbstractDbRepository(String url, String username, String password, Validator<E> validator) {
        super(validator);
        this.url = url;
        this.username = username;
        this.password = password;
        try{
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection(url, username, password);
            System.out.println("Conexiune efectuate cu suptces!");
            loadData();
        } catch (Exception e){
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
        }
    }



    abstract protected void loadData();


    abstract ResultSet savePreparedStatement(E entity) throws SQLException;

    /**
     * Functia intoarce un id daca acesta trebuie setat ca fiind unul dat din baza de date
     * @param x
     * @return id-ul care trebuie setat la entitate
     *          sau null daca nu trebuie setat niciun id
     */
    abstract ID getIdFromDb(ResultSet x);

    @Override
    public E save(E entity) throws ValidationException {
        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        if(entities.get(entity.getId()) != null) {
            return entity;
        }
        else {
            ResultSet idDb = null;
            try {

                idDb = savePreparedStatement(entity); //cateodata vreau sa primesc cateodata nu
                ID id = getIdFromDb(idDb);
                if(id != null)
                    entity.setId(id);

            } catch (SQLException e) {
                //System.out.println("RIP");
            } finally {
                entities.put(entity.getId(),entity);
            }

        }
        return null;
    }

    abstract PreparedStatement deletePreparedStatement(ID id) throws SQLException;

    @Override
    public E delete(ID id) {
        E result = super.delete(id);
        if(result != null){
            try{
                PreparedStatement ps = deletePreparedStatement(id);
                ps.execute();
            } catch (SQLException e){
                System.out.println("RIP");
            }

        }
        return result;
    }

    abstract PreparedStatement updatePreparedStatement(E entity) throws SQLException;

    @Override
    public E update(E entity) {
        E result = super.update(entity);
        if(result == null){
            try{
                PreparedStatement ps = updatePreparedStatement(entity);
                if(ps != null)
                    ps.execute();
            } catch (SQLException e){

            }
        }
        return result;
    }
}
