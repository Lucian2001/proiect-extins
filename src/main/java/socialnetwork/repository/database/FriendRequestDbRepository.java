package socialnetwork.repository.database;

import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.FriendRequestStatus;
import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.sql.Timestamp;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.time.LocalDateTime;

public class FriendRequestDbRepository extends AbstractDbRepository<Tuple<Long, Long>, FriendRequest>{
    public FriendRequestDbRepository(String url, String username, String password, Validator<FriendRequest> validator) {
        super(url, username, password, validator);
    }

    @Override
    protected void loadData() {
        try{
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM friend_requests");
            while ( rs.next() ) {
                Long user1_id = rs.getLong("user_from_id");
                Long user2_id = rs.getLong("user_to_id");
                Integer s = rs.getInt("status");
                FriendRequestStatus status = null;
                if(s == 0){
                    status = FriendRequestStatus.pending;
                } else if(s == 1){
                    status = FriendRequestStatus.approved;
                } else if(s == 2){
                    status = FriendRequestStatus.rejected;
                }
                LocalDateTime date = rs.getTimestamp("date").toLocalDateTime();
                FriendRequest friendRequest = new FriendRequest();
                friendRequest.setStatus(status);
                friendRequest.setDate(date);
                friendRequest.setId(new Tuple<>(user1_id, user2_id));
                entities.put(friendRequest.getId(), friendRequest);
            }

            rs.close();
            stmt.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    ResultSet savePreparedStatement(FriendRequest entity) throws SQLException {

        String insert = "INSERT INTO friend_requests(user_from_id, user_to_id, status, date) VALUES (? , ?, ?, ?);";
        PreparedStatement ps = c.prepareStatement(insert);
        ps.setLong(1, entity.getId().getLeft());
        ps.setLong(2,  entity.getId().getRight());

        if(entity.getStatus() == FriendRequestStatus.approved){
            ps.setInt(3, 1);
        } else if(entity.getStatus() == FriendRequestStatus.rejected){
            ps.setInt(3, 2);
        } else {
            ps.setInt(3, 0);
        }
        ps.setTimestamp(4, Timestamp.valueOf(entity.getDate()));
        return ps.executeQuery();
    }

    @Override
    Tuple<Long, Long> getIdFromDb(ResultSet x) {
        return null;
    }

    @Override
    PreparedStatement deletePreparedStatement(Tuple<Long, Long> id) throws SQLException {
        String delete = "DELETE FROM friend_requests WHERE user_from_id = ? and user_to_id = ?";
        PreparedStatement ps = c.prepareStatement(delete);
        ps.setLong(1, id.getLeft());
        ps.setLong(2, id.getRight());
        return ps;
    }

    @Override
    PreparedStatement updatePreparedStatement(FriendRequest friendRequest) throws SQLException {
        String insert = "UPDATE friend_requests SET status = ? WHERE user_from_id = ? and user_to_id = ?";
        PreparedStatement ps = c.prepareStatement(insert);
        if(friendRequest.getStatus() == FriendRequestStatus.approved)
            ps.setInt(1, 1);
        else
            ps.setInt(1, 2);
        ps.setLong(2, friendRequest.getId().getLeft());
        ps.setLong(3, friendRequest.getId().getRight());
        return ps;
    }


}
