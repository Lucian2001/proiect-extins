package socialnetwork.repository.database;

import jdk.vm.ci.meta.Local;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.sql.*;
import java.time.LocalDateTime;

public class MessageDbRepository extends AbstractDbRepository<Long, Message> {

    public MessageDbRepository(String url, String username, String password, Validator<Message> validator) {
        super(url, username, password, validator);
    }

    @Override
    protected void loadData() {
        try{
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM messages");
            while ( rs.next() ) {
                Long id = rs.getLong("id");
                Long reply_to = rs.getLong("reply_to");
                String messageText = rs.getString("text");
                Long userFrom = rs.getLong("user_from");
                Long groupID = rs.getLong("group_id");
                LocalDateTime date = rs.getTimestamp("time").toLocalDateTime();
                Message message = new Message(userFrom, groupID, messageText);
                message.setId(id);
                message.setDate(date);
                if (reply_to == 0)
                    message.setReply(null);
                else
                    message.setReply(reply_to);
                entities.put(message.getId(), message);
            }

            rs.close();
            stmt.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    ResultSet savePreparedStatement(Message entity) throws SQLException {
        String insert = "INSERT INTO messages(time, text, reply_to, user_from, group_id)  VALUES (?, ?, ?, ?, ?) RETURNING id";
        PreparedStatement ps = c.prepareStatement(insert);
        ps.setTimestamp(1, Timestamp.valueOf(entity.getDate()));
        ps.setString(2, entity.getMessage());
        if(entity.getReply() == null){
            ps.setNull(3, Types.INTEGER);
        } else {
            ps.setInt(3, entity.getReply().intValue());
        }
        ps.setInt(4, entity.getFrom().intValue());
        ps.setInt(5, entity.getTo().intValue());
        return ps.executeQuery();
    }

    @Override
    PreparedStatement updatePreparedStatement(Message aLong) throws SQLException {
        return null;
    }

    @Override
    Long getIdFromDb(ResultSet x) {
        try {
            x.next();
            Long id = x.getLong(1);
            return id;
        } catch (SQLException e){

        }
        return null;
    }

    @Override
    PreparedStatement deletePreparedStatement(Long aLong) throws SQLException {
        return null;
    }
}
