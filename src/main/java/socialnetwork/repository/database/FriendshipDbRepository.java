package socialnetwork.repository.database;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.validators.Validator;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;
import java.time.LocalDateTime;

public class FriendshipDbRepository extends AbstractDbRepository<Tuple<Long, Long>, Prietenie> {

    public FriendshipDbRepository(String url, String username, String password, Validator<Prietenie> validator) {
        super(url, username, password, validator);
    }

    @Override
    protected void loadData() {
        try{
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM friendships");
            while ( rs.next() ) {
                Long user1_id = rs.getLong("user1_id");
                Long user2_id = rs.getLong("user2_id");
                LocalDateTime date = rs.getDate("date").toLocalDate().atStartOfDay();
                Prietenie friendship = new Prietenie();
                friendship.setDate(date);
                friendship.setId(new Tuple<>(user1_id, user2_id));
                entities.put(friendship.getId(), friendship);
            }

            rs.close();
            stmt.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    ResultSet savePreparedStatement(Prietenie entity) throws SQLException {
        String insert = "INSERT INTO friendships(user1_id, user2_id, date) VALUES (? , ?, ?);";
        PreparedStatement ps = c.prepareStatement(insert);
        ps.setLong(1, entity.getId().getLeft());
        ps.setLong(2,  entity.getId().getRight());
        java.sql.Date sqlDate = java.sql.Date.valueOf( entity.getDate().toLocalDate());
        ps.setDate(3, sqlDate);
        return ps.executeQuery();
    }

    @Override
    Tuple<Long, Long> getIdFromDb(ResultSet x) {
        return null;
    }

    @Override
    PreparedStatement deletePreparedStatement(Tuple<Long, Long> id) throws SQLException {
        String delete = "DELETE FROM friendships WHERE user1_id = ? and user2_id = ?";
        PreparedStatement ps = c.prepareStatement(delete);
        ps.setLong(1, id.getLeft());
        ps.setLong(2, id.getRight());
        return ps;
    }

    @Override
    PreparedStatement updatePreparedStatement(Prietenie entity) throws SQLException {
        return null;
    }
}
