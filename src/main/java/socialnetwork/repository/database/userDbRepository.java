package socialnetwork.repository.database;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSet;

public class userDbRepository extends AbstractDbRepository<Long, Utilizator> {
    public userDbRepository(String url, String username, String password, Validator<Utilizator> validator) {
        super(url, username, password, validator);
    }


    @Override
    protected void loadData() {
        try{
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM users");
            while ( rs.next() ) {
                Long id = rs.getLong("id");
                String  firstName = rs.getString("first_name");
                String  lastName = rs.getString("last_name");
                String mail = rs.getString("mail");
                String password = rs.getString("password");
                Utilizator user = new Utilizator(firstName, lastName, mail, password);
                user.setId(id);
                entities.put(user.getId(), user);
            }

            rs.close();
            stmt.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    ResultSet savePreparedStatement(Utilizator user) throws SQLException{
        String insert = "INSERT INTO users(first_name, last_name, mail, password)  VALUES (?, ?, ?, ?) RETURNING id";
        PreparedStatement ps = c.prepareStatement(insert);
        ps.setString(1, user.getFirstName());
        ps.setString(2, user.getLastName());
        ps.setString(3, user.getMail());
        ps.setString(4, user.getPassword());

        return ps.executeQuery();
    }

    @Override
    PreparedStatement deletePreparedStatement(Long id) throws SQLException {
        String delete = "DELETE FROM users WHERE ID = ?";
        PreparedStatement ps = c.prepareStatement(delete);
        ps.setInt(1, id.intValue());
        return ps;
    }

    @Override
    PreparedStatement updatePreparedStatement(Utilizator aLong) throws SQLException {
        return null;
    }

    @Override
    Long getIdFromDb(ResultSet x) {
        try {
            x.next();
            Long id = x.getLong(1);
            return id;
        } catch (SQLException e){

        }
        return null;
    }
}
