package socialnetwork.repository.database;

import socialnetwork.domain.Group;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class GroupsDbRepository extends AbstractDbRepository<Long, Group> {
    public GroupsDbRepository(String url, String username, String password, Validator<Group> validator) {
        super(url, username, password, validator);

    }

    @Override
    protected void loadData() {
        try{
            Statement stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery("SELECT * FROM groups");
            while ( rs.next() ) {
                Long id = rs.getLong("id");
                Long idLastMessage = rs.getLong("last_message");
                List<Long> users = new ArrayList<>();
                Statement stmtUsers = c.createStatement();
                ResultSet usersRs = stmtUsers.executeQuery("SELECT user_id FROM groups_users WHERE group_id = " + id);
                while(usersRs.next()){
                    users.add(usersRs.getLong("user_id"));
                }
                Group group = new Group(users);
                group.setId(id);
                if(idLastMessage != 0)
                    group.setLastMessage(idLastMessage);
                entities.put(group.getId(), group);
            }

            rs.close();
            stmt.close();
        } catch (Exception e){
            System.out.println(e);
        }
    }

    @Override
    ResultSet savePreparedStatement(Group group) throws SQLException {
        String insert = "INSERT INTO groups DEFAULT VALUES RETURNING id";
        PreparedStatement ps = c.prepareStatement(insert, ResultSet.TYPE_SCROLL_INSENSITIVE,
                ResultSet.CONCUR_READ_ONLY);
        ResultSet idGroup = ps.executeQuery();
        idGroup.next();
        int idGroupCurrent = idGroup.getInt(1);
        for(Long user:group.getUsers()){
            String insert1 = "INSERT INTO groups_users(user_id, group_id) VALUES (?, ?)";
            ps = c.prepareStatement(insert1);
            ps.setInt(1, user.intValue());
            ps.setInt(2, idGroupCurrent);
            ps.execute();
        }
        idGroup.previous();
        return idGroup;
    }

    @Override
    Long getIdFromDb(ResultSet x) {
        try {
            x.next();
            Long id = x.getLong(1);
            return id;
        } catch (SQLException e){

        }
        return null;
    }

    @Override
    PreparedStatement deletePreparedStatement(Long aLong) throws SQLException {
        return null;
    }

    @Override
    PreparedStatement updatePreparedStatement(Group group) throws SQLException {
        PreparedStatement psUpdate = null;
        if(group.getLastMessage() != null) {
            String insert = "UPDATE groups SET last_message = ? WHERE id = ?";
            psUpdate = c.prepareStatement(insert);
            psUpdate.setInt(1, group.getLastMessage().intValue());
            psUpdate.setLong(2, group.getId());
        }
        PreparedStatement ps = null;
        Statement stmtUsers = c.createStatement();
        stmtUsers.execute("DELETE FROM groups_users WHERE group_id = " + group.getId());
        for(Long user:group.getUsers()){
            String insert1 = "INSERT INTO groups_users(user_id, group_id) VALUES (?, ?)";
            ps = c.prepareStatement(insert1);
            ps.setInt(1, user.intValue());
            ps.setInt(2, group.getId().intValue());
            ps.execute();
        }


        return psUpdate;
    }


}
