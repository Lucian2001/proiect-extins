package socialnetwork.repository.paging.repositories;

import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.Validator;
import socialnetwork.repository.paging.*;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class UserDbRepository extends AbstractPageRepository<Long, Utilizator> {


    public UserDbRepository(String url, String username, String password, Validator<Utilizator> validator) {
        super(url, username, password, validator);
    }

    @Override
    protected Page<Utilizator> findPage(Pageable pageable, Long userId) {
        String select = "SELECT Distinct * FROM users " +
                "LEFT JOIN friendships f on users.id = f.user1_id or users.id = f.user2_id " +
                "WHERE (users.id != ? and users.id != ?) and ((f.user1_id IS NULL or f.user2_id is NULL)\n" +
                "or (f.user1_id != ? and f.user2_id != ?)) LIMIT ? OFFSET ?";

        List<Utilizator> users = new ArrayList<>();
        try {
            PreparedStatement ps = c.prepareStatement(select);
            ps.setInt(1, userId.intValue());
            ps.setInt(2, userId.intValue());
            ps.setInt(3, userId.intValue());
            ps.setInt(4, userId.intValue());
            ps.setInt(5, pageable.getPageSize());
            ps.setInt(6, pageable.getPageNumber() * pageable.getPageSize());
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                Utilizator user = new Utilizator(result.getString("first_name"), result.getString("last_name"), result.getString("mail"), result.getString("password"));
                user.setId(result.getLong("id"));
                users.add(user);
            }
        }
        catch (SQLException ex){
            return null;
        }
        return new DataPage<Utilizator>(pageable, users.stream());
    }

    @Override
    public Utilizator findOneDB(Long userId) throws SQLException {
        String select = "SELECT * FROM users WHERE id = ?";
        PreparedStatement ps = c.prepareStatement(select);
        ps.setLong(1, userId);
        ResultSet result = ps.executeQuery();
        if (result.next()) {
            Utilizator user = new Utilizator(result.getString("first_name"), result.getString("last_name"), result.getString("mail"), result.getString("password"));
            user.setId(userId);
            return user;
        }
        else{
            return null;
        }
    }

    @Override
    protected Utilizator saveDB(Utilizator user) throws SQLException {
        String insert = "INSERT INTO users(first_name, last_name, mail, password)  VALUES (?, ?, ?, ?) RETURNING id";
        PreparedStatement ps = c.prepareStatement(insert);
        ps.setString(1, user.getFirstName());
        ps.setString(2, user.getLastName());
        ps.setString(3, user.getMail());
        ps.setString(4, user.getPassword());
        ResultSet result =  ps.executeQuery();
        result.next();
        Long id = result.getLong(1);
        user.setId(id);
        return user;
    }

    @Override
    protected Utilizator deleteFromDb(Long userId) throws SQLException {
        String delete = "DELETE FROM users WHERE ID = ? RETURNING *";
        PreparedStatement ps = c.prepareStatement(delete);
        ps.setInt(1, userId.intValue());
        ResultSet result = ps.executeQuery();
        if(result.next()){
            Utilizator user = new Utilizator(result.getString("first_name"), result.getString("last_name"), result.getString("mail"), result.getString("password"));
            user.setId(userId);
            return user;
        } else {
            return null;
        }
    }

    @Override
    protected void updateDB(Utilizator entity) throws SQLException {

    }

    @Override
    public Iterable<Utilizator> findAll() {
        String select = "SELECT * FROM users";
        List<Utilizator> users = new ArrayList<>();
        try {
            PreparedStatement ps = c.prepareStatement(select);
            ResultSet result = ps.executeQuery();
            while (result.next()) {
                Utilizator user = new Utilizator(result.getString("first_name"), result.getString("last_name"), result.getString("mail"), result.getString("password"));
                users.add(user);
            }
        }
        catch (SQLException ex){
            return null;
        }
        return users;
    }

    @Override
    public Long getId() {
        return null;
    }
}
