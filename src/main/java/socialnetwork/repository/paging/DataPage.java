package socialnetwork.repository.paging;

import java.util.stream.Stream;

public class DataPage<E> implements Page<E> {
    private Pageable pageable;
    private Stream<E> content;

    public DataPage(Pageable pageable, Stream<E> content) {
        this.pageable = pageable;
        this.content = content;
    }

    @Override
    public Pageable getPageable() {
        return this.pageable;
    }

    @Override
    public Pageable nextPageable() {
        return new Pageable(this.pageable.getPageNumber() + 1, this.pageable.getPageSize());
    }

    @Override
    public Stream<E> getContent() {
        return this.content;
    }
}
