package socialnetwork.repository.paging;

import socialnetwork.domain.Entity;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.domain.validators.Validator;

import java.sql.*;

public abstract class AbstractPageRepository<ID, E extends Entity<ID>> implements PagingRepository<ID, E> {

    protected Validator<E> validator;
    private String url;
    private String username;
    private String password;

    protected Connection c = null;
    public AbstractPageRepository(String url, String username, String password, Validator<E> validator) {
        this.validator = validator;
        this.url = url;
        this.username = username;
        this.password = password;
        try{
            Class.forName("org.postgresql.Driver");
            c = DriverManager.getConnection(url, username, password);
            System.out.println("Conexiune efectuate cu suptces!");
        } catch (Exception e){
            e.printStackTrace();
            System.err.println(e.getClass().getName()+": "+e.getMessage());
        }
    }

    protected abstract Page<E> findPage(Pageable pageable,Long userId);

    @Override
    public Page<E> findAll(Pageable pageable, Long userId) {
        return findPage(pageable, userId);
    }


    protected abstract E findOneDB(ID id) throws SQLException;

    @Override
    public E findOne(ID id) {
        try{
            return findOneDB(id);
        } catch (SQLException e){
            System.out.println("RIP");
            return null;
        }
    }



    protected abstract E saveDB(E entity) throws SQLException;




    @Override
    public E save(E entity) throws ValidationException {
        if (entity==null)
            throw new IllegalArgumentException("entity must be not null");
        validator.validate(entity);
        try {
            entity = saveDB(entity); //cateodata vreau sa primesc cateodata nu
            return null;
        } catch (SQLException e) {
            //System.out.println("RIP");
            return entity;
        }
    }

    protected abstract E deleteFromDb(ID id) throws SQLException;

    @Override
    public E delete(ID id) {
        if (id == null){
            throw new IllegalArgumentException("id must be not null");
        }

            try{
                return deleteFromDb(id);
            } catch (SQLException e){

                System.out.println("RIP");
                return null;
            }
    }

    protected abstract void updateDB(E entity) throws SQLException;

    @Override
    public E update(E entity) {
        if(entity == null)
            throw new IllegalArgumentException("entity must be not null!");
        validator.validate(entity);
        try{
            updateDB(entity);
            return null;
        } catch (SQLException e){
            return entity;
        }
    }
}
