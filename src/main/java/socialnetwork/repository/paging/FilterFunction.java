package socialnetwork.repository.paging;

@FunctionalInterface
public interface FilterFunction<E> {
    boolean filter(E object);
}
