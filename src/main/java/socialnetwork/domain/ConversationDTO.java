package socialnetwork.domain;

import java.util.ArrayList;
import java.util.List;
import java.util.Stack;

public class ConversationDTO {
    List<Message> messages = new ArrayList<>();

    public ConversationDTO() {

    }
    public void addMessage(Message message){
        messages.add(0, message);
    }
    public void addNewMessage(Message message){
        messages.add(message);
    }

    @Override
    public String toString() {
        return messages.toString();
    }

    public List<Message> getMessages() {
        return messages;
    }
}
