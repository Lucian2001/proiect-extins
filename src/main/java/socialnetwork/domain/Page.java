package socialnetwork.domain;

import socialnetwork.service.FriendRequestService;
import socialnetwork.service.UtilizatorService;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Queue;

public class Page {
    private Long idUser;
    private String firstName;
    private String lastName;
    private List<FriendDTO> friends;
    private List<FriendRequestDTO>  friendRequestsSent;
    private List<FriendRequestDTO>  friendRequestsReceived;
    private HashMap<Long, ConversationDTO> activeConversations;

    public Page(Long idUser, String firstName, String lastName, List<FriendDTO> friends, List<FriendRequestDTO> friendRequestsSent, List<FriendRequestDTO> friendRequestsReceived) {
        this.idUser = idUser;
        this.firstName = firstName;
        this.lastName = lastName;
        this.friends = friends;
        this.friendRequestsSent = friendRequestsSent;
        this.friendRequestsReceived = friendRequestsReceived;
        activeConversations = new HashMap<>();
    }

    public Long getIdUser() {
        return idUser;
    }

    public void setIdUser(Long idUser) {
        this.idUser = idUser;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<FriendDTO> getFriends() {
        return friends;
    }

    public void setFriends(List<FriendDTO> friends) {
        this.friends = friends;
    }

    public List<FriendRequestDTO> getFriendRequestsSent() {
        return friendRequestsSent;
    }

    public void setFriendRequestsSent(List<FriendRequestDTO> friendRequestsSent) {
        this.friendRequestsSent = friendRequestsSent;
    }

    public List<FriendRequestDTO> getFriendRequestsReceived() {
        return friendRequestsReceived;
    }

    public void setFriendRequestsReceived(List<FriendRequestDTO> friendRequestsReceived) {
        this.friendRequestsReceived = friendRequestsReceived;
    }

    public void addConversation(Long id, ConversationDTO x){
            activeConversations.put(id, x);
    }

    public ConversationDTO getConversation(Long id){
        return activeConversations.get(id);
    }

    public void addMessageToConversation(Long id, Message msg) {
        activeConversations.get(id).addNewMessage(msg);
    }
}
