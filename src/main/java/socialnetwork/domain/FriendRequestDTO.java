package socialnetwork.domain;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class FriendRequestDTO {
    private String firstName;
    private String lastName;
    private String status;
    private LocalDateTime date;
    private Tuple<Long, Long> id;

    public FriendRequestDTO(Tuple<Long, Long> id, String firstName, String lastName, FriendRequestStatus status, LocalDateTime date) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        switch (status){
            case approved:
                this.status = "Aprobata";
                break;
            case rejected:
                this.status = "Declinata";
                break;
            default:
                this.status = "In asteptare";
                break;
        }

        this.date = date;
    }

    @Override
    public String toString() {
        return this.lastName + " " + this.firstName + " "
                + date.format(DateTimeFormatter.ofPattern("yyyy-MM-dd")) + " "
                + status;
    }

    public Tuple<Long, Long> getId() {
        return id;
    }
}
