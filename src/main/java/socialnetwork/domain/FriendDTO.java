package socialnetwork.domain;

import java.time.LocalDateTime;

public class FriendDTO extends Entity<Long> implements DTO{
    String friendFirstName, friendLastName;
    LocalDateTime date;

    public FriendDTO(String friendFirstName, String friendLastName, LocalDateTime date) {
        this.friendFirstName = friendFirstName;
        this.friendLastName = friendLastName;
        this.date = date;
    }

    public String getName() {
        return friendFirstName + " " + friendLastName;
    }

    @Override
    public String toString() {
        return friendFirstName + " " + friendLastName + " " + date;
    }
}
