package socialnetwork.domain;

import java.util.List;

public class Group extends Entity<Long>{
    Long lastMessage = null;
    List<Long> users;

    public Group(List<Long> users) {

        this.users = users;
    }

    public void setLastMessage(Long lastMessage) {
        this.lastMessage = lastMessage;
    }

    public Long getLastMessage() {
        return lastMessage;
    }

    @Override
    public String toString() {
        return users.toString();
    }

    public List<Long> getUsers() {
        return users;
    }

    public boolean verifica(Long idFrom) {
        for(Long user:users){
            if (idFrom == user)
                return true;
        }
        return false;
    }
}
