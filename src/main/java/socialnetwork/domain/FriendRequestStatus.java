package socialnetwork.domain;

public enum FriendRequestStatus {
    pending,
    approved,
    rejected
}
