package socialnetwork.domain.validators;

import socialnetwork.domain.Utilizator;

public class UtilizatorValidator implements Validator<Utilizator> {
    @Override
    public void validate(Utilizator entity) throws ValidationException {
        String error ="";
        if(!entity.getMail().matches("^(.+)@(.+)$"))
            error+= "Adresa invalida!";
        if(entity.getFirstName().isEmpty() ||  !entity.getFirstName().matches("^[a-zA-Z]*$")){
            error += "Prenume incorect!";
        }
        if(entity.getLastName().isEmpty() || !entity.getLastName().matches("^[a-zA-Z]*$")){
            error += "Nume incorect!";
        }

        if(!error.isEmpty())
            throw new ValidationException(error);
    }
}
