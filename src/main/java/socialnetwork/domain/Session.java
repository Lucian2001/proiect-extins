package socialnetwork.domain;

public class Session {
    Long id;
    static Session instance = new Session();

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    private Session() {
    }

    public static Session getInstance(){
        return instance;
    }

}
