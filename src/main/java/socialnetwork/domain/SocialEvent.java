package socialnetwork.domain;

import java.time.LocalDateTime;

public class SocialEvent extends Entity<Long>{
    String name, image, description;
    Long creator;
    Long group;
    LocalDateTime date;

    public SocialEvent(String name, String image, String description, Long creator, Long group, LocalDateTime date) {
        this.name = name;
        this.image = image;
        this.description = description;
        this.creator = creator;
        this.date = date;
        this.group = group;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getCreator() {
        return creator;
    }

    public void setCreator(Long creator) {
        this.creator = creator;
    }

    public Long getGroup() {
        return group;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public void setGroup(Long group) {
        this.group = group;
    }

    @Override
    public String toString() {
        return  name;
    }
}
