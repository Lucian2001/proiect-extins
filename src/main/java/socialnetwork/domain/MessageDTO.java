package socialnetwork.domain;

import java.time.LocalDateTime;

public class MessageDTO implements DTO{
    String friendFirstName, friendLastName, message;
    LocalDateTime date;

    public MessageDTO(String friendFirstName, String friendLastName, String message, LocalDateTime date) {
        this.friendFirstName = friendFirstName;
        this.friendLastName = friendLastName;
        this.date = date;
        this.message = message;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String toString() {
        return friendFirstName + " " + friendLastName + ":-" + message + " " +  date;
    }
}
