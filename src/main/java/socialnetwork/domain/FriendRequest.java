package socialnetwork.domain;

import java.time.LocalDateTime;

public class FriendRequest extends Entity<Tuple<Long, Long>>{
    FriendRequestStatus status;
    LocalDateTime date;

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }

    public FriendRequest() {
        status = FriendRequestStatus.pending;
        date = LocalDateTime.now();
    }

    public FriendRequestStatus getStatus() {
        return status;
    }

    public void setStatus(FriendRequestStatus status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "Cerere de prietenie trimisa de la " + this.getId().getLeft();
    }


}
