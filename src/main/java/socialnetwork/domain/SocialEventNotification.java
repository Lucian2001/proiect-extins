package socialnetwork.domain;


import java.time.LocalDateTime;

public class SocialEventNotification extends Entity<Long>{
    LocalDateTime eventDate;
    String socialEventName;
    Long socialEvent;
    Long user;

    public SocialEventNotification(LocalDateTime eventDate, Long user, Long socialEvent, String socialEventName) {
        this.eventDate = eventDate;
        this.socialEvent = socialEvent;
        this.user = user;
        this.socialEventName = socialEventName;
    }

    public LocalDateTime getEventDate() {
        return eventDate;
    }

    public Long getSocialEvent() {
        return socialEvent;
    }

    public Long getUser() {
        return user;
    }

    @Override
    public String toString() {
        return "Mai sunt " + String.valueOf(eventDate.getDayOfYear() - LocalDateTime.now().getDayOfYear()) + " zile pana la " +
                "evenimentul " + socialEventName;
    }
}
