package fxinject;

import socialnetwork.fxinject.Inject;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Parameter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * A very simple container that is used to store dependency information of
 * various objects. In addition the dependency container provides methods for
 * creating new objects and injecting stored dependencies (so called Dependency
 * Injection).
 * 
 * <p>
 * Dependency container allows to implement a simple <i>inversion of control</i>
 * pattern, in which an external class is responsible for obtaining, creating
 * and passing variables to an instance upon its initialization. This is in
 * contrast to the "normal" pattern, in which an object obtains or creates the
 * variables itself thus ending up depending on the implementation thereof (thus
 * the name <i>dependency</i>).
 * </p>
 * 
 * 
 * @author Denis Zhidkikh
 * @version 1.2.2017
 * @see Inject
 *
 */
public class DependencyContainer {
    private Map<String, Object> instanceDependencies;
    private Map<Class<?>, Class<?>> classDependency;

    /**
     * Creates an empty instance of the dependency container.
     */
    public DependencyContainer() {
        instanceDependencies = new HashMap<>();
        classDependency = new HashMap<>();
    }

    /**
     * Adds a dependency in form of an instance.
     * 
     * <p>
     * When the dependency container will perform dependency injection and finds
     * an injectable field or constructor with a parameter of <b>the same
     * <i>name</i></b> as specified, it will inject the given <i>dependency</i>
     * into it.
     * </p>
     * 
     * <p>
     * Use this method if you have an object that is static, unique and other
     * worker classes depend on (i.e. a service or a database).
     * </p>
     * 
     * @param name
     *            The name of the dependency. This is used to match fields and
     *            constructor parameters named using {@link Inject} annotation.
     * @param dependency
     *            The object that will be injected.
     */
    public void addInstanceDependency(String name, Object dependency) {
        instanceDependencies.put(name, dependency);
    }

    /**
     * Adds a dependency in form of a class type.
     * 
     * <p>
     * During dependency injection, if an injectable field or constructor with a
     * parameter of the same type as <i>superClass</i> is found, a new instance
     * of <i>implementationClass</i> is created and injected into the field or
     * parameter.
     * </p>
     * 
     * <p>
     * Use this if you need to bind a superclass (i.e. an abstract class or an
     * interface) to a class that implements it. That way you can specify which
     * implementations of the superclass are used to create an object.
     * </p>
     * 
     * @param superClass
     *            A type or an interface that should be bound to the class that
     *            implements it.
     * @param implementationClass
     *            The class that implements properties the superclass.
     */
    public <T> void addDependency(Class<T> superClass, Class<? extends T> implementationClass) {
        classDependency.put(superClass, implementationClass);
    }

    /**
     * Attempts to construct an instance of the provided class. Injects values
     * into the instance if there are any to inject.
     * 
     * @param clazz
     *            The class of the object that should be created.
     * @return An instance of the class with dependencies injected. If an
     *         instance could not be created, prints a stack trace and returns
     *         <b>null</b>.
     */
    @SuppressWarnings("unchecked")
    public <T> T createObject(Class<T> clazz) {
        if (clazz == null)
            return null;
        T result;
        List<Constructor<?>> constructors = Arrays.stream(clazz.getConstructors()).filter(c -> {
            if (c.getAnnotation(Inject.class) == null)
                return false;
            String[] paramNames = c.getAnnotation(Inject.class).value();
            Parameter[] parameters = c.getParameters();
            for (int i = 0; i < parameters.length; i++) {
                if (!instanceDependencies.containsKey(i < paramNames.length ? paramNames[i] : parameters[i].getName())
                        && !classDependency.containsKey((parameters[i].getType())))
                    return false;
            }
            return true;
        }).collect(Collectors.toList());

        if (constructors.size() > 0) {
            Constructor<?> c = constructors.get(0);
            String[] paramNames = c.getAnnotation(Inject.class).value();
            Object[] params = new Object[c.getParameterCount()];
            Parameter[] parameters = c.getParameters();
            for (int i = 0; i < parameters.length; i++) {
                Parameter param = parameters[i];
                String paramName = i < paramNames.length ? paramNames[i] : param.getName();
                params[i] = instanceDependencies.get(paramName);
                if (params[i] == null)
                    params[i] = createObject(classDependency.get(param.getType()));
            }
            try {
                result = (T) c.newInstance(params);
            } catch (InstantiationException | IllegalAccessException | IllegalArgumentException
                    | InvocationTargetException e) {
                e.printStackTrace();
                return null;
            }
        } else
            try {
                result = clazz.newInstance();
            } catch (InstantiationException | IllegalAccessException e) {
                e.printStackTrace();
                return null;
            }

        Field[] fields = result.getClass().getDeclaredFields();
        for (int i = 0; i < fields.length; i++) {
            Field field = fields[i];
            if (field.getAnnotation(Inject.class) != null) {
                String[] paramNames = field.getAnnotation(Inject.class).value();
                Object dependency = instanceDependencies.get(paramNames.length > 0 ? paramNames[0] : field.getName());
                if (dependency == null || !dependency.getClass().equals(field.getType()))
                    dependency = createObject(classDependency.get(field.getType()));
                if (dependency != null) {
                    field.setAccessible(true);
                    try {
                        field.set(result, dependency);
                    } catch (IllegalArgumentException | IllegalAccessException e) {
                        e.printStackTrace();
                        return null;
                    }
                    field.setAccessible(false);
                }
            }
        }
        return result;
    }
}
