package socialnetwork.fxinject;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * The {@link Inject} annotation is used to mark fields or constructors that
 * require dependency injection.
 * 
 * <p>
 * The annotation allows to specify one or multiple name aliases. The aliases
 * can be used to give a name to a field or <b>all</b> constructor parameters
 * even if name preservation for parameters is disabled.
 * </p>
 * 
 * <p>
 * If used on a field, a dependency container will inject an instance of the
 * same name (or type) into it, provided an instance with such name is defined
 * in the container. <b>If a name alias is specified</b>, the dependency
 * container will use it instead of the field's own name.
 * </p>
 * 
 * <p>
 * If used on a constructor, a dependency container will attempt to inject
 * instances into all parameters. <b>You must use name aliases</b> if you want
 * to use instance dependency injection instead of just class dependency
 * injection.
 * </p>
 * 
 * @author Denis Zhidkikh
 * @version 1.2.2017
 * @see DependencyContainer
 *
 */
@Target({ ElementType.FIELD, ElementType.CONSTRUCTOR })
@Retention(RetentionPolicy.RUNTIME)
public @interface Inject {
    /**
     * Name aliases array. If empty, the dependency container will use field's
     * own names to search for suitable instance dependencies.
     * 
     * @return Name alias array.
     */
    String[] value() default {};
}
