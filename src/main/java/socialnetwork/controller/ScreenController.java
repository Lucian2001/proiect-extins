package socialnetwork.controller;


import fxinject.DependencyContainer;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;

import java.io.IOException;
import java.util.HashMap;

public class ScreenController {
    private HashMap<String, String> screenMap = new HashMap<>();
    private Scene main = null;
    private static ScreenController instance = new ScreenController();
    private FXMLLoader loader;
    private DependencyContainer container = null;
    private ScreenController() {

    }

    public Scene getMainScene(){
        return main;
    }

    public void setContainer(DependencyContainer container ) {
        this.container = container;
    }
    public void addScreen(String name, String location)  {

        screenMap.put(name, location);

    }



    public  void removeScreen(String name){
        screenMap.remove(name);
    }

    public void activate(String name) throws IOException {
        loader = new FXMLLoader();
        loader.setLocation(getClass().getResource(screenMap.get(name)));
        loader.setControllerFactory(container::createObject);
        Pane pane = loader.load();
        if(main == null){
            main = new Scene(pane, 1200, 900);
        } else {
            main.setRoot(pane);
        }
    }

    public static ScreenController get_instance(){
        return instance;
    }

}
