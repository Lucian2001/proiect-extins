package socialnetwork.controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.HBox;
import javafx.util.Callback;
import socialnetwork.domain.ConversationDTO;
import socialnetwork.domain.Message;
import socialnetwork.fxinject.Inject;
import socialnetwork.service.PageService;
import socialnetwork.utils.Event;
import socialnetwork.utils.EventObject;
import socialnetwork.utils.Observer;
import socialnetwork.utils.OpenChatEvent;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

final class MessageListViewCell extends ListCell<Message> {

    Long currentUser;
    public MessageListViewCell(Long currentId) {
        currentUser = currentId;
    }

    public void setCurrentUser(Long currentUser) {
        this.currentUser = currentUser;
    }

    @Override
    protected void updateItem(Message item, boolean empty) {
        super.updateItem(item, empty);
        if (empty) {
            setGraphic(null);
        } else {
            // Create the HBox
            HBox hBox = new HBox();
            if(currentUser != item.getFrom()) {
                hBox.setAlignment(Pos.CENTER_RIGHT);
            } else{
                hBox.setAlignment(Pos.CENTER_LEFT);
            }
            // Create centered Label
            Label label = new Label(item.getMessage());
            label.setAlignment(Pos.CENTER);

            hBox.getChildren().add(label);
            setGraphic(hBox);
        }
    }
}


public class chatController implements Observer {
    @Inject
    PageService pageService;

    ObservableList<Message> modelMessages = FXCollections.observableArrayList();
    @FXML
    AnchorPane messageWindow;
    @FXML
    ToolBar chatTabsToolBar;
    @FXML
    ListView<Message> listViewMessages;
    @FXML
    Label labelMessage;
    @FXML
    TextField textFieldMessage;

    Long currentFriendId = -1L;

    @FXML
    public void initialize() {
        messageWindow.setDisable(true);
        pageService.addObserver(this);
        messageWindow.setVisible(false);
        listViewMessages.setItems(modelMessages);

    }

    @FXML
    public void handleSendMessage(){
        modelMessages.add(pageService.sendMessage(currentFriendId, textFieldMessage.getText()));
        textFieldMessage.clear();
    }

    public void addMessageTab(String name, Long idFriend){
        Button b = new Button(name){
            @Override
            public int hashCode() {
                return super.hashCode();
            }

            @Override
            public boolean equals(Object obj) {
                try {
                    return this.getText().equals(((Button) obj).getText());
                }catch (ClassCastException e){
                    return super.equals(obj);
                }

            }
        };
        b.setOnAction(a -> loadConversation(name, idFriend));
        if(!chatTabsToolBar.getItems().contains(b)){
            if(chatTabsToolBar.getItems().size() == 6){
                chatTabsToolBar.getItems().remove(0);
                chatTabsToolBar.getItems().add(0, b);
            } else{
                chatTabsToolBar.getItems().add(b);
            }

        }
    }

    private void loadConversation(String nameFriend, Long id){
        messageWindow.setDisable(false);
        messageWindow.setVisible(true);
        labelMessage.setText(nameFriend);
        currentFriendId = id;
        listViewMessages.setCellFactory(stringListView -> new MessageListViewCell(currentFriendId));
        modelMessages.clear();
        List<Message> z = pageService.getConversation(id).getMessages();
        modelMessages.addAll(z );


    }


    @Override
    public void update(Event<? extends EventObject> e) {
        switch (e.getEvent().getType()){
            case OPEN_CHAT:
                OpenChatEvent x = (OpenChatEvent) e.getEvent();
                addMessageTab(x.getName(), x.getIdFriend());
                loadConversation(x.getName(), x.getIdFriend());
                break;
        }
    }

    @FXML
    public void handleCloseMessageTab() {
        messageWindow.setDisable(true);
        messageWindow.setVisible(false);
    }
}
