package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import socialnetwork.fxinject.Inject;
import socialnetwork.service.PageService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.service.exceptions.ServiceException;
import socialnetwork.utils.MessageAlert;


public class LoginController {

    @Inject
    PageService pageService;
    @Inject
    UtilizatorService userService;

    @FXML
    private TextField textFieldUserId;
    @FXML
    private  TextField textFieldPassword;
    @FXML
    private Button buttonLogin;

    @FXML
    public void initialize() {

    }

    @FXML
    public void handleLogin(){
        String mail = textFieldUserId.getText();
        String password = textFieldPassword.getText();
        try{
            if (mail.equals("admin")){
                ScreenController.get_instance().activate("adminPageScreen");
                return;
           }
            Long id = Long.parseLong(textFieldUserId.getText());
            //Long id = userService.login(mail, password);
           pageService.init(id);
           ScreenController.get_instance().activate("mainScreen");
        } catch (ServiceException ex){
            MessageAlert.showMessage(null, Alert.AlertType.ERROR, "Login", ex.getMessage());
        }
        catch (Exception ex){
            System.out.println(ex.getMessage());
        }
    }

    public void handleRegister() {
        try{
            ScreenController.get_instance().activate("registerScreen");
        } catch (Exception ex){
            System.out.println("Boss wtf!");
        }
    }
}


