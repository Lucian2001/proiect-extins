package socialnetwork.controller;

import com.sun.jdi.InvocationException;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;
import socialnetwork.controller.tab_controller.TabAllUsersController;
import socialnetwork.controller.tab_controller.TabFriendRequests;
import socialnetwork.controller.tab_controller.TabFriends;
import socialnetwork.domain.SocialEvent;
import socialnetwork.domain.SocialEventNotification;
import socialnetwork.fxinject.Inject;
import socialnetwork.service.FriendRequestService;
import socialnetwork.service.PageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.*;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainController implements Observer {



    @Inject
    PageService pageService;

    ObservableList<SocialEvent> model = FXCollections.observableArrayList();
    ObservableList<SocialEventNotification> modelNotifications = FXCollections.observableArrayList();
    ListView<SocialEvent> listViewSocialEvents = new ListView<>();
    ListView<SocialEventNotification> listViewNotifications = new ListView<>();
    Boolean isSearchActive = false;
    SocialEvent currentSocialEvent = null;
    Boolean isSubsribedToNotification;
    Boolean isShowNotificationActive = false;
    @FXML
    AnchorPane searchPane;
    @FXML
    Button buttonJoinEvent;
    @FXML
    TextField textFieldSearchEvent;

    @FXML
    ImageView imageViewEventImage;

    @FXML
    Label labelName;

    @FXML
    Label labelDate;

    @FXML
    Button buttonNotification;
    @FXML
    Button buttonShowNotifications;
    @FXML
    Label labelDescription;

    public void initialize() throws FileNotFoundException {
        pageService.addObserver(this);
        buttonJoinEvent.setVisible(false);
        buttonJoinEvent.setDisable(true);
        loadImage("default.png");
        pageService.getDefaultSocialEvent();

    }

    @Override
    public void update(Event<? extends EventObject> e) {
        switch (e.getEvent().getType()){
            case OPEN_EVENT:
                OpenSocialEventEvent event = (OpenSocialEventEvent) e.getEvent();
                loadSocialEvent(event.getSocialEvent());
                if(event.getPartOfSocialEvent()) {
                    buttonJoinEvent.setVisible(false);
                    buttonJoinEvent.setDisable(true);
                } else{
                    buttonJoinEvent.setVisible(true);
                    buttonJoinEvent.setDisable(false);
                }
                searchPane.getChildren().remove(listViewSocialEvents);
                isSearchActive = false;
                currentSocialEvent = event.getSocialEvent();
                isSubsribedToNotification = pageService.isSubscribed(currentSocialEvent);
                if(isSubsribedToNotification){
                    buttonNotification.setText("Dezactiveaza notificarile");
                } else{
                    buttonNotification.setText("Activeaza notificarile");
                }
                break;
        }
    }

    public void handleCreateEvent(ActionEvent actionEvent) {
        try {
            ScreenController.get_instance().activate("addEventScreen");
        } catch(Exception ex) {

        }
    }


    public void handleSearchEvent(MouseEvent mouseEvent) {
        if(!isSearchActive) {
            model.clear();
            listViewSocialEvents.setItems(model);
            model.addAll(pageService.getSocialEvents());
            listViewSocialEvents.setLayoutX(textFieldSearchEvent.getLayoutX());
            listViewSocialEvents.setLayoutY(textFieldSearchEvent.getLayoutY() + textFieldSearchEvent.getHeight() + 1);
            searchPane.getChildren().add(listViewSocialEvents);
            listViewSocialEvents.setOnMouseClicked(new EventHandler<MouseEvent>() {
                @Override
                public void handle(MouseEvent mouseEvent) {
                    if (listViewSocialEvents.getSelectionModel().getSelectedItem() != null) {
                        pageService.openSocialEvent(listViewSocialEvents.getSelectionModel().getSelectedItem(), false);
                    }
                }
            });
            isSearchActive = true;
        } else{
            searchPane.getChildren().remove(listViewSocialEvents);
            isSearchActive = false;
        }
    }

    private void loadImage(String fileName){
        FileInputStream inputstream = null;
        try {
            inputstream = new FileInputStream("src/main/resources/views/main/" + fileName);
            Image image = new Image(inputstream);
            imageViewEventImage.setImage(image);
        }
        catch(Exception ex){
            System.out.println("Dsa");
        }

    }

    private void loadSocialEvent(SocialEvent event){
        labelName.setText(event.getName());
        labelDate.setText(event.getDate().format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
        labelDescription.setText(event.getDescription());
        loadImage(event.getImage());
    }


    public void handleJoinEvent(ActionEvent actionEvent) {
        buttonJoinEvent.setDisable(true);
        buttonJoinEvent.setVisible(false);
        pageService.joinSocialEvent(currentSocialEvent);
    }

    public void handleAddNotification(ActionEvent actionEvent) {
        if(isSubsribedToNotification){
            isSubsribedToNotification = false;
            buttonNotification.setText("Activeaza notificarile");
            pageService.deleteNotification(currentSocialEvent);
        } else{
            isSubsribedToNotification = true;
            buttonNotification.setText("Dezactiveaza notificarile");
            pageService.addNotification(currentSocialEvent);
        }
    }

    public void handleShowNotifications(ActionEvent actionEvent) {
        if(!isShowNotificationActive) {
            modelNotifications.clear();
            listViewNotifications.setItems(modelNotifications);
            modelNotifications.addAll(pageService.getNotifications());
            listViewNotifications.setLayoutX(buttonShowNotifications.getLayoutX());
            listViewNotifications.setLayoutY(buttonShowNotifications.getLayoutY() + buttonShowNotifications.getHeight() + 1);
            listViewNotifications.setMinWidth(285);
            searchPane.getChildren().add(listViewNotifications);

            isShowNotificationActive = true;
        } else{
            searchPane.getChildren().remove(listViewNotifications);
            isShowNotificationActive = false;
        }
    }

    public void handleLogOut(ActionEvent actionEvent) {
        pageService.logOut();
    }
}
