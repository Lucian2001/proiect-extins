package socialnetwork.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import socialnetwork.domain.ConversationDTO;
import socialnetwork.domain.Message;
import socialnetwork.fxinject.Inject;
import socialnetwork.service.PageService;
import socialnetwork.utils.*;

public class ChatGroupController implements Observer {

    public Button buttonGroupChat;
    @Inject
    PageService pageService;

    @FXML
    TextArea textAreaGroupChat;
    @FXML
    TextField textFieldGroupChat;


    private Long currentGroupId = 1L;
    @FXML
    public void initialize(){
        pageService.addObserver(this);
      //  textAreaGroupChat.setStyle("-fx-opacity: 1;");
      //  textAreaGroupChat.setStyle("-fx-font-alignment: center");
      //  loadConversation();
    }

    @Override
    public void update(Event<? extends EventObject> e) {
        switch (e.getEvent().getType()){
            case OPEN_EVENT:
                textAreaGroupChat.clear();
                OpenSocialEventEvent event = (OpenSocialEventEvent) e.getEvent();
                currentGroupId = event.getSocialEvent().getGroup();
                if(event.getPartOfSocialEvent())
                    loadConversation();
                break;
            case JOIN_EVENT:
                loadConversation();
                break;
        }
    }
    private void loadConversation(){
        ConversationDTO conversation = pageService.getGroupConversation(currentGroupId);
        String conversationText ="";
        for(Message message:conversation.getMessages()){
            String msg = pageService.getUser(message.getFrom()).getLastName()
                    + " " + pageService.getUser(message.getFrom()).getFirstName()
                    + ":- " + message.getMessage();
            msg = msg.replaceAll("(.{" + "50" + "})", "$1\n").trim();
            msg += '\n';
            conversationText += msg;
        }
        textAreaGroupChat.setText(conversationText);
    }

    @FXML
    private void handleSendMessage(){
        pageService.sendMessageGroup(currentGroupId, textFieldGroupChat.getText());
        loadConversation();
        textFieldGroupChat.clear();
    }
}
