package socialnetwork.controller.tab_controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.ScrollEvent;
import socialnetwork.domain.FriendRequestDTO;
import socialnetwork.domain.Utilizator;
import socialnetwork.fxinject.Inject;
import socialnetwork.service.PageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;
import socialnetwork.utils.Event;
import socialnetwork.utils.EventObject;
import socialnetwork.utils.Observable;
import socialnetwork.utils.Observer;

import java.net.BindException;
import java.util.List;
import java.util.stream.Collectors;


public class TabAllUsersController implements Observer {
    @Inject
    PageService pageService;
    private int currentPage = 0;

    private int pageSize = 38;
    private int index;
    List<Utilizator> usersPage1;
    List<Utilizator> usersPage2;
    @Override
    public void update(Event<? extends EventObject> e) {
        switch (e.getEvent().getType()){
            case FRIEND_REQUEST_ACCEPT:
            case SEND_FRIEND_REQUEST:
           //     load_non_friend_users();
                break;
            case DELETE_FRIEND:
             //   load_non_friend_users();
                break;
            case RETRACT_FRIEND_REQUEST:
             //   load_non_friend_users();
                break;
            default:
                break;
        }
    }

    ObservableList<Utilizator> model = FXCollections.observableArrayList();

    @FXML
    private ListView<Utilizator> listViewUsers;



    @FXML
    public void initialize() {
        listViewUsers.setItems(model);
        load_non_friend_users();
        initContextMenu();
        pageService.addObserver(this);
        scrollEvent();
    }

    public void initContextMenu(){
        ContextMenu contextMenu = new ContextMenu();
        MenuItem sendFriendRequest = new MenuItem("Trimite cerere de prietenie!");
        contextMenu.getItems().add(sendFriendRequest);
        listViewUsers.setContextMenu(contextMenu);
        sendFriendRequest.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                Utilizator user = listViewUsers.getSelectionModel().getSelectedItem();
                pageService.sendFriendRequest(user.getId());
            }
        });
    }

    private void load_non_friend_users(){
        usersPage1 = pageService.getAllNonFriends(currentPage, pageSize);
        usersPage2 = pageService.getAllNonFriends(currentPage + 1, pageSize);
        model.setAll(usersPage1);
        currentPage++;

    }

    private void scrollEvent(){
        listViewUsers.setOnScroll(new EventHandler<ScrollEvent>() {
            @Override
            public void handle(ScrollEvent scrollEvent) {

                if(scrollEvent.getTextDeltaY() > 0){
                    //Up
                    if(index> 0){
                        index -=1;
                        model.remove(pageSize - 1);
                        model.add(0, usersPage1.get(index));

                    } else if(currentPage > 1){
                        System.out.println("dsa");
                        index = pageSize;
                        usersPage2 = usersPage1;
                        currentPage--;
                        usersPage1 = pageService.getAllNonFriends(currentPage - 1, pageSize);
                        index -=1;
                        model.remove(pageSize - 1);
                        model.add(0, usersPage1.get(index));
                    }
                }
                else{
                    //Down
                    if(index < usersPage2.size()){
                        model.remove(0);
                        model.add(usersPage2.get(index));
                        index += 1;
                    } else if(pageService.getAllNonFriends(currentPage + 1, pageSize).size() > 0){
                        usersPage1 = usersPage2;
                        currentPage++;
                        usersPage2 = pageService.getAllNonFriends(currentPage, pageSize);
                        index = 0;
                        model.remove(0);
                        model.add(usersPage2.get(index));
                        index += 1;
                    }
                }
            }
        });
    }


}
