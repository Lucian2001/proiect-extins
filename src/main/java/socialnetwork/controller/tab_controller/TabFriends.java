package socialnetwork.controller.tab_controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Alert;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import socialnetwork.domain.FriendDTO;
import socialnetwork.domain.FriendRequestDTO;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.fxinject.Inject;
import socialnetwork.service.PageService;
import socialnetwork.utils.*;

import java.util.List;


public class TabFriends implements Observer {

    private int currentPage = 0;
    private int pageSize = 37;
    @Inject
    PageService pageService;

    ObservableList<FriendDTO> model = FXCollections.observableArrayList();
    @FXML
    ListView<FriendDTO> listViewFriends;

    @FXML
    public void initialize() {
        pageService.addObserver(this);
        listViewFriends.setItems(model);
        initFriends();

    }

    void initFriends(){
        List<FriendDTO> friends = pageService.getFriends(currentPage, pageSize);
        model.setAll(friends);
        initContextMenu();
    }

    @Override
    public void update(Event<? extends EventObject> e) {
        switch (e.getEvent().getType()){
            case FRIEND_REQUEST_ACCEPT:
                initFriends();
                break;
            case DELETE_FRIEND:
                initFriends();
                break;
            default:
                break;
        }
    }

    public void initContextMenu(){
        ContextMenu contextMenu = new ContextMenu();
        MenuItem deleteFriend = new MenuItem("Sterge prieten!");
        MenuItem openChat = new MenuItem("Open chat!");

        openChat.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FriendDTO friend = listViewFriends.getSelectionModel().getSelectedItem();
                pageService.openChat(friend.getId());
            }
        });

        deleteFriend.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FriendDTO friend = listViewFriends.getSelectionModel().getSelectedItem();
                pageService.deleteFriend(friend.getId());
                MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Stergere prieten!", "Stergerea a fost efectuata cu succes!");
            }
        });

        contextMenu.getItems().add(deleteFriend);
        contextMenu.getItems().add(openChat);
        listViewFriends.setContextMenu(contextMenu);
    }

    public void handlePreviousPage(ActionEvent actionEvent) {
        if(currentPage > 0) {
            currentPage = currentPage - 1;
            initFriends();
        }
    }

    public void handleNextPage(ActionEvent actionEvent) {
        if( pageService.getFriends(currentPage+1, pageSize).size() > 0) {
            currentPage++;
            initFriends();
        }
    }
}
