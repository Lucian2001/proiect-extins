package socialnetwork.controller.tab_controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.ListView;
import javafx.scene.control.MenuItem;
import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.FriendRequestDTO;
import socialnetwork.domain.Utilizator;
import socialnetwork.fxinject.Inject;
import socialnetwork.service.PageService;
import socialnetwork.utils.Event;
import socialnetwork.utils.EventObject;
import socialnetwork.utils.Observable;
import socialnetwork.utils.Observer;

public class TabFriendRequests implements Observer {

    ObservableList<FriendRequestDTO> modelReceived = FXCollections.observableArrayList();
    ObservableList<FriendRequestDTO> modelSent = FXCollections.observableArrayList();
    @Inject
    PageService pageService;
    @FXML
    ListView<FriendRequestDTO> listViewSent;
    @FXML
    ListView<FriendRequestDTO> listViewReceived;

    @Override
    public void update(Event<? extends EventObject> e) {
        switch (e.getEvent().getType()){
            case FRIEND_REQUEST_ACCEPT:
            case FRIEND_REQUEST_REJECT :
                modelReceived.setAll(pageService.getFriendRequestsReceived());
                break;
            case SEND_FRIEND_REQUEST:
                modelSent.setAll(pageService.getFriendRequestsSent());
            case DELETE_FRIEND:
                modelReceived.setAll(pageService.getFriendRequestsReceived());
                modelSent.setAll(pageService.getFriendRequestsSent());
                break;
            case RETRACT_FRIEND_REQUEST:
                modelSent.setAll(pageService.getFriendRequestsSent());
                break;
            default:
                break;
        }
    }

    @FXML
    public void initialize(){
        listViewReceived.setItems(modelReceived);
        listViewSent.setItems(modelSent);
        init_models();
        initContextMenuReceived();
        initContextMenuSent();
        pageService.addObserver(this);
    }

    private void init_models(){
        modelReceived.setAll(pageService.getFriendRequestsReceived());
        modelSent.setAll(pageService.getFriendRequestsSent());
    }

    public void initContextMenuSent(){
        ContextMenu contextMenuSent = new ContextMenu();
        MenuItem retractFriendRequest = new MenuItem("Retrage cererea de prietenie!");
        retractFriendRequest.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FriendRequestDTO friend = listViewSent.getSelectionModel().getSelectedItem();
                pageService.retractFriendRequest(friend.getId().getRight());
            }
        });
        contextMenuSent.getItems().add(retractFriendRequest);
        listViewSent.setContextMenu(contextMenuSent);
    }

    public void initContextMenuReceived(){
        ContextMenu contextMenuReceived = new ContextMenu();
        MenuItem acceptFriendRequest = new MenuItem("Accepta cererea de prietenie!");
        MenuItem rejectFriendRequest = new MenuItem("Declina cererea de prietenie!");
        acceptFriendRequest.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FriendRequestDTO friend = listViewReceived.getSelectionModel().getSelectedItem();
                pageService.solveFriendRequest(friend.getId().getLeft(), 1);
            }
        });
        rejectFriendRequest.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                FriendRequestDTO friend = listViewReceived.getSelectionModel().getSelectedItem();
                pageService.solveFriendRequest(friend.getId().getLeft(), 2);
            }
        });

        contextMenuReceived.getItems().add(acceptFriendRequest);
        contextMenuReceived.getItems().add(rejectFriendRequest);
        listViewReceived.setContextMenu(contextMenuReceived);
    }
}
