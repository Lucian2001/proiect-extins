package socialnetwork.controller.tab_controller;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import socialnetwork.domain.SocialEvent;
import socialnetwork.domain.Utilizator;
import socialnetwork.fxinject.Inject;
import socialnetwork.service.PageService;
import socialnetwork.utils.Event;
import socialnetwork.utils.EventObject;
import socialnetwork.utils.Observer;

public class TabMyEvents implements Observer {
    @Inject
    PageService pageService;

    ObservableList<SocialEvent> model = FXCollections.observableArrayList();
    @FXML
    ListView<SocialEvent> listViewMyEvents;

    @FXML
    void initialize(){
        pageService.addObserver(this);
        listViewMyEvents.setItems(model);
        loadEvents();
        listViewMyEvents.setOnMouseClicked(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent mouseEvent) {
                if(listViewMyEvents.getSelectionModel().getSelectedItem() != null){
                    pageService.openSocialEvent(listViewMyEvents.getSelectionModel().getSelectedItem(), true);
                }
            }
        });
    }

    void loadEvents(){
        model.addAll(pageService.getUserSocialEvents());
    }


    @Override
    public void update(Event<? extends EventObject> e) {
        switch (e.getEvent().getType()){
            case JOIN_EVENT:
                model.clear();
                loadEvents();
                break;
        }
    }
}
