package socialnetwork.controller;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.print.*;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.transform.Scale;


import socialnetwork.domain.*;
import socialnetwork.fxinject.Inject;
import socialnetwork.service.MessageService;
import socialnetwork.service.PrietenieService;
import socialnetwork.service.UtilizatorService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class AdminPageController {

    ObservableList<PieChart.Data> pieChartData = FXCollections.observableArrayList();

    @Inject
    UtilizatorService userService;
    @Inject
    PrietenieService friendshipService;
    @Inject
    MessageService messageService;
    @FXML
    TextField textFieldUser;

    @FXML
    TextField textFieldFriend;

    @FXML
    ListView<DTO> listView;

    @FXML
    BarChart<String, Number> barChartMessages;

    @FXML
    PieChart pieChartFriends;

    List<LocalDateTime> dates = new ArrayList<>();

    @FXML
    public void initialize(){
        dates.add(LocalDateTime.of(2020, 8, 1, 1, 1));
        dates.add(LocalDateTime.of(2020, 9, 1, 1, 1));
        dates.add(LocalDateTime.of(2020, 10, 1, 1, 1));
        dates.add(LocalDateTime.of(2020, 11, 1, 1, 1));
        dates.add(LocalDateTime.of(2020, 12, 1, 1, 1));
        pieChartFriends.setData(pieChartData);


    }


    void setPieChartFriends(Long id){
        pieChartFriends.getData().clear();
        List<List<FriendDTO>> listsFriendsPerMonth = new ArrayList<>();
        for(LocalDateTime date: dates){
            List<FriendDTO> x= friendshipService.getUserFriendsOnSpecificDate(id, date.getMonth().getValue(), date.getYear());
            listsFriendsPerMonth.add(x);
            PieChart.Data data = new PieChart.Data(date.getMonth().toString()+" " + date.getYear() + "-" + x.size() + " prieteni", x.size());
            pieChartData.add(data);
            data.getNode().addEventHandler(MouseEvent.MOUSE_CLICKED,
                    new EventHandler<MouseEvent>() {
                        @Override public void handle(MouseEvent e) {
                            ObservableList<DTO>  friends = FXCollections.observableArrayList(x);
                            listView.setItems(friends);
                        }
                    });
        }
        pieChartFriends.setLabelsVisible(false);

    }

    public void setBarChartMessages(Long id, Long idFriend) {
        barChartMessages.getData().clear();
        barChartMessages.setTitle("Mesaje primite de utilizator");
        barChartMessages.getXAxis().setAnimated(false);
        barChartMessages.getYAxis().setAnimated(false);
        XYChart.Series<String, Number> series = new XYChart.Series<String, Number>();
        series.getData().clear();
        barChartMessages.getData().add(series);
        for(LocalDateTime date: dates) {
            List<MessageDTO> x = messageService.getMessagesReceivedByUserOnSpecificDate(id, date.getMonth().getValue(), date.getYear(), idFriend);
            XYChart.Data<String, Number> data = new XYChart.Data<String, Number>(date.getMonth().toString()+" " + date.getYear(), x.size());
            series.getData().add(data);
            data.getNode().setOnMouseClicked(new EventHandler<MouseEvent>() {
               @Override
               public void handle(MouseEvent event) {
                   ObservableList<DTO>  messages = FXCollections.observableArrayList(x);
                   listView.setItems(messages);
               }
            });

        }


        barChartMessages.setLegendVisible(false);
    }

    @FXML
    void handleButton(){
        listView.getItems().clear();
        String user, friend;
        user = textFieldUser.getText();
        friend = textFieldFriend.getText();
        try {
            if (friend.isEmpty() && !user.isEmpty()) {
                setBarChartMessages(Long.parseLong(user), 0L);
                setPieChartFriends(Long.parseLong(user));
            } else if(!user.isEmpty()){
                setBarChartMessages(Long.parseLong(user), Long.parseLong(friend));
                setPieChartFriends(Long.parseLong(user));
            }

        } catch (Exception ex){
            System.out.println("Dsa");
        }
    }

    public void handleSavePdf(ActionEvent actionEvent) {
        try {
            List<String> months = new ArrayList<>();
            months.add("August");
            months.add("Septembrie");
            months.add("Octombrie");
            months.add("Noiembrie");
            months.add("Decembrie");

            Document document = new Document();
            PdfWriter.getInstance(document, new FileOutputStream("raport.pdf"));
            document.open();
            if(!textFieldUser.getText().equals("")){
                Paragraph paragraph = new Paragraph("Pentru userul" + " " + userService.findOne(Long.parseLong(textFieldUser.getText())));
                paragraph.setAlignment(Element.ALIGN_LEFT);
                document.add(paragraph);
                paragraph = new Paragraph("Prietenii noi");
                paragraph.setAlignment(Element.ALIGN_LEFT);
                document.add(Chunk.NEWLINE);
                document.add(paragraph);
                document.add(Chunk.NEWLINE);


                PdfPTable table = new PdfPTable(2);

                PdfPCell month = new PdfPCell(new Phrase("Luna"));
                month.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(month);

                PdfPCell friend = new PdfPCell(new Phrase("Nume"));
                friend.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(friend);

                table.setHeaderRows(1);
                int i = 0;
                for(LocalDateTime date: dates) {
                    List<FriendDTO> x= friendshipService.getUserFriendsOnSpecificDate(Long.parseLong(textFieldUser.getText()), date.getMonth().getValue(), date.getYear());
                    for(FriendDTO friendDTO:x){
                        table.addCell(months.get(i));
                        table.addCell(friendDTO.getName());
                    }
                    i++;
                }

                document.add(table);
            }

            if(!textFieldFriend.getText().equals("")){
                document.add(Chunk.NEWLINE);
                Paragraph paragraph = new Paragraph("Mesaje cu prietenul " + " " + userService.findOne(Long.parseLong(textFieldFriend.getText())));
                paragraph.setAlignment(Element.ALIGN_LEFT);
                document.add(paragraph);
                paragraph.setAlignment(Element.ALIGN_LEFT);
                document.add(Chunk.NEWLINE);
                PdfPTable table = new PdfPTable(2);

                PdfPCell month = new PdfPCell(new Phrase("Luna"));
                month.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(month);

                PdfPCell friend = new PdfPCell(new Phrase("Nume"));
                friend.setHorizontalAlignment(Element.ALIGN_CENTER);
                table.addCell(friend);

                table.setHeaderRows(1);
                int i = 0;
                for(LocalDateTime date: dates) {
                    List<MessageDTO> x= messageService.getMessagesReceivedByUserOnSpecificDate(Long.parseLong(textFieldUser.getText()), date.getMonth().getValue(), date.getYear(), Long.parseLong(textFieldFriend.getText()));
                    for(MessageDTO messageDTO:x){
                        table.addCell(months.get(i));
                        table.addCell(messageDTO.getMessage());
                    }
                    i++;
                }

                document.add(table);

            }
            document.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
