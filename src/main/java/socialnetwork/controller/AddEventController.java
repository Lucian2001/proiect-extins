package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.DatePicker;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import socialnetwork.fxinject.Inject;
import socialnetwork.service.PageService;


import java.io.File;
import java.time.LocalDateTime;

public class AddEventController {

    @Inject
    PageService pageService;
    @FXML
    TextArea textAreaDescriere;
    @FXML
    DatePicker datePickerDateEvent;

    @FXML
    TextField textFieldName;

    File selectedImage = null;
    @FXML
    public void initialize(){

    }
    public void handleBack(ActionEvent actionEvent) {
        try {
            ScreenController.get_instance().activate("mainScreen");
        } catch(Exception ex) {

        }
    }

    public void handleAddEvent(ActionEvent actionEvent) {
        String name, description;
        LocalDateTime date;
        name = textFieldName.getText();
        description = textAreaDescriere.getText();
        date = datePickerDateEvent.getValue().atStartOfDay();
        if(selectedImage.isFile() && selectedImage.getName().matches("([^\\s]+(\\.(?i)(jpe?g|png|gif|bmp))$)")){
            pageService.addEvent(name, description, selectedImage.getName(), date);
        }

    }

    @FXML
    public void handleSelectFile(ActionEvent actionEvent) {
        FileChooser fileChooser = new FileChooser();
        Stage newWindow = new Stage();
        newWindow.setTitle("Second Stage");
        selectedImage = fileChooser.showOpenDialog(newWindow);

    }
}
