package socialnetwork.controller;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.fxinject.Inject;
import socialnetwork.service.PageService;
import socialnetwork.service.UtilizatorService;


public class RegisterController {

    @Inject
    UtilizatorService userService;
    @Inject
    PageService pageService;

    @FXML
    private TextField textFieldMail;
    @FXML
    private TextField textFieldName;
    @FXML
    private TextField textFieldSurname;
    @FXML
    private TextField textFieldPassword1;
    @FXML
    private TextField textFieldPassword2;
    @FXML
    private void initialize(){
        
    }
    
    @FXML
    public void handleRegister() {
        String mail, name, surname, password1, password2;
        mail = textFieldMail.getText();
        name = textFieldName.getText();
        surname = textFieldSurname.getText();
        password1 = textFieldPassword1.getText();
        password2 = textFieldPassword2.getText();
        if(password1.equals(password2))
            userService.addUser(surname, name, mail, password1);
        else
            throw new ValidationException("Parolele nu sunt la fel");
        try{
            ScreenController.get_instance().activate("loginScreen");
        } catch (Exception ex){
            System.out.println("Boss wtf!");
        }


    }

    public void handleBack() {
        try{
            ScreenController.get_instance().activate("loginScreen");
        } catch (Exception ex){
            System.out.println("Boss wtf!");
        }
    }
}
