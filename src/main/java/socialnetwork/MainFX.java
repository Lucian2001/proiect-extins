package socialnetwork;
import fxinject.DependencyContainer;
import javafx.application.Application;
import javafx.stage.Stage;
import socialnetwork.config.ApplicationContext;
import socialnetwork.controller.ScreenController;
import socialnetwork.domain.*;
import socialnetwork.domain.validators.*;
import socialnetwork.repository.database.*;
import socialnetwork.repository.paging.AbstractPageRepository;
import socialnetwork.repository.paging.repositories.UserDbRepository;
import socialnetwork.service.*;

import java.io.IOException;


public class MainFX extends Application {
    public void init_objects() throws IOException, ClassNotFoundException {
        String userFileName= ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.users");
        String friendshipFileName = ApplicationContext.getPROPERTIES().getProperty("data.socialnetwork.friendships");
        // String fileName="data/users.csv";

        AbstractPageRepository<Long, Utilizator> userDbRepository = new UserDbRepository(ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password"), new UtilizatorValidator());

        AbstractDbRepository<Tuple<Long, Long>, Prietenie> friendshipDbRepository = new FriendshipDbRepository(ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password"), new PrietenieValidator());
        AbstractDbRepository<Tuple<Long, Long>, FriendRequest> friendRequestDbRepository = new FriendRequestDbRepository(ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password"), new FriendRequestValidator());


        AbstractDbRepository<Long, Group> groupsDbRepository = new GroupsDbRepository(ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password"), new GroupValidator());

        AbstractDbRepository<Long, Message> messageDbRepository = new MessageDbRepository(ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password"), new MessageValidator());

        AbstractDbRepository<Long, SocialEvent> socialEventDbRepository = new SocialEventDbRepository(ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password"), new SocialEventValidator());

        AbstractDbRepository<Long, SocialEventNotification> socialEventNotificationDbRepository = new SocialEventNotificationDbRepository(ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.url"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.username"),
                ApplicationContext.getPROPERTIES().getProperty("database.socialnetwork.password"), new SocialEventNotificationValidator());



        DependencyContainer container = new DependencyContainer();
        ComunityService comunityService = new ComunityService(userDbRepository);
        UtilizatorService userService = new UtilizatorService(userDbRepository , friendshipDbRepository, comunityService);
        GroupsService groupsService = new GroupsService(groupsDbRepository, userDbRepository, messageDbRepository);
        PrietenieService friendshipService = new PrietenieService(friendshipDbRepository, userDbRepository , comunityService, friendRequestDbRepository, groupsService);
        FriendRequestService friendRequestService = new FriendRequestService(friendRequestDbRepository, userDbRepository, friendshipService);
        SocialEventService socialEventService = new SocialEventService(socialEventDbRepository, groupsService);
        SocialEventNotificationService socialEventNotificationService = new SocialEventNotificationService(socialEventNotificationDbRepository);

        MessageService messageService = new MessageService(messageDbRepository, userDbRepository, groupsDbRepository, groupsService);
        PageService pageService = new PageService(userService, friendRequestService, friendshipService,groupsService, messageService, socialEventService, socialEventNotificationService);

        container.addInstanceDependency("pageService", pageService);
        container.addInstanceDependency("userService", userService);
        container.addInstanceDependency("friendshipService", friendshipService);
        container.addInstanceDependency("messageService", messageService);

        ScreenController.get_instance().setContainer(container);
    }

    @Override
    public void start(Stage primaryStage) throws Exception{
        init_objects();
        ScreenController.get_instance().addScreen("loginScreen", "/views/loginView.fxml");
        ScreenController.get_instance().addScreen("mainScreen","/views/main/mainView.fxml");
        ScreenController.get_instance().addScreen("registerScreen", "/views/registerView.fxml");
        ScreenController.get_instance().addScreen("adminPageScreen", "/views/adminPage.fxml");
        ScreenController.get_instance().addScreen("addEventScreen", "/views/main/addEvent.fxml");
        ScreenController.get_instance().activate("loginScreen");
        primaryStage.setScene(ScreenController.get_instance().getMainScene());
        primaryStage.setTitle("SocialNetwork");
        primaryStage.setWidth(1400);
        primaryStage.setResizable(false);
        primaryStage.show();

    }

    public static void main(String[] args) {
        launch(args);
    }

}


