package socialnetwork.utils;

public class Event<T extends EventObject>{
    T event;

    public Event(T event) {
            this.event = event;
    }

    public T getEvent(){
            return event;
            }
}
