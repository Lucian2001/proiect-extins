package socialnetwork.utils;

import socialnetwork.domain.ConversationDTO;

public class OpenChatEvent implements EventObject{
    EventType type;
    String name;
    Long idFriend;

    public OpenChatEvent(EventType type, String name, Long idFriend) {
        this.type = type;
        this.name = name;
        this.idFriend = idFriend;
    }

    @Override
    public EventType getType() {
        return type;
    }



    public String getName() {
        return name;
    }

    public Long getIdFriend() {
        return idFriend;
    }
}
