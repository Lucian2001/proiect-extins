package socialnetwork.utils;

import socialnetwork.domain.SocialEvent;

public class OpenSocialEventEvent implements EventObject{
    EventType type;
    SocialEvent socialEvent;
    Boolean isPartOfSocialEvent;

    public Boolean getPartOfSocialEvent() {
        return isPartOfSocialEvent;
    }

    public OpenSocialEventEvent(EventType type, SocialEvent socialEvent, Boolean isPartOfSocialEvent) {
        this.type = type;
        this.socialEvent = socialEvent;
        this.isPartOfSocialEvent = isPartOfSocialEvent;
    }

    @Override
    public EventType getType() {
        return type;
    }

    public SocialEvent getSocialEvent() {
        return socialEvent;
    }
}
