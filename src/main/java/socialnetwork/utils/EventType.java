package socialnetwork.utils;

public enum EventType {
        FRIEND_REQUEST_ACCEPT,
        FRIEND_REQUEST_REJECT,
        SEND_FRIEND_REQUEST,
        DELETE_FRIEND,
        RETRACT_FRIEND_REQUEST,
        OPEN_CHAT,
        OPEN_EVENT,
        JOIN_EVENT
}
