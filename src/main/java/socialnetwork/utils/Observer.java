package socialnetwork.utils;

import java.util.Optional;

public interface Observer {
    void update(Event<? extends EventObject> e);
}