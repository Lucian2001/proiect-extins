package socialnetwork.utils;

public interface EventObject {
    abstract EventType getType();

}
