package socialnetwork.utils;

public class CommonEvent implements EventObject{
    EventType type;

    public CommonEvent(EventType type) {
        this.type = type;
    }

    @Override
    public EventType getType() {
        return type;
    }
}
