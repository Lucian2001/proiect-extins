package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.repository.Repository;
import socialnetwork.service.exceptions.ServiceException;
import sun.nio.ch.Util;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class MessageService {
    Repository<Long, Message> repo;
    Repository<Long, Utilizator> repoUsers;
    Repository<Long, Group> repoGroups;
    GroupsService groupsService;

    Long idCurent = 1L;

    public MessageService(Repository<Long, Message> repo, Repository<Long, Utilizator> repuUsers, Repository<Long, Group> repoGroups, GroupsService groupsService) {
        this.repo = repo;
        this.repoUsers = repuUsers;
        this.repoGroups = repoGroups;
        this.groupsService = groupsService;
    }

    public Message sendMessage(Long idFrom, Long idTo, String message){
        Group to = repoGroups.findOne(idTo);
        if (to.verifica(idFrom) == false){
            throw new ServiceException("Userul nu face parte din grup!");
        }
        Message newMessage = null;
        if(to != null) {
             newMessage = new Message(idFrom, idTo, message);
            idCurent++;
            newMessage.setReply(to.getLastMessage());
            repo.save(newMessage);
            groupsService.insertNewMessage(to, newMessage);

        } else{
            throw new ServiceException("Grupul nu a fost gasit!");
        }
        return newMessage;
    }

    public Message sendChatMessage(Long idUser1, Long idUser2, String message){
        Optional<Group> group = StreamSupport.stream(repoGroups.findAll().spliterator(), true)
                .filter(g-> (g.getUsers().size() == 2) &&((g.getUsers().get(0).equals(idUser1) && g.getUsers().get(1).equals(idUser2))
                        || (g.getUsers().get(1).equals(idUser1) && g.getUsers().get(0).equals(idUser2))) ).findAny();        Group to = null;
        if(group.isPresent()){
            to = group.get();
        }
        if (to.verifica(idUser1) == false){
            throw new ServiceException("Userul nu face parte din grup!");
        }
        Message newMessage = null;
        if(to != null) {
            newMessage = new Message(idUser1, to.getId(), message);
            idCurent++;
            newMessage.setReply(to.getLastMessage());
            repo.save(newMessage);
            groupsService.insertNewMessage(to, newMessage);

        } else{
            throw new ServiceException("Grupul nu a fost gasit!");
        }
        return newMessage;
    }

    public List<MessageDTO> getMessagesReceivedByUserOnSpecificDate(Long id, Integer month, Integer year,Long idFriend){

        List<Group> groupsUserIs = StreamSupport.stream(repoGroups.findAll().spliterator(), true)
                .filter(group -> {
                    for(Long userGroup:group.getUsers()){
                        if(id == userGroup){
                            return true;
                        }
                    }
                    return false;
                })
                .collect(Collectors.toList());



        LocalDate date = LocalDate.of(year, month, 1);
        Iterable<Message> messages= repo.findAll();
        return StreamSupport.stream(messages.spliterator(), true)
                .filter(message -> !message.getFrom().equals(id))
                .filter(message -> {
                    for(Group group:groupsUserIs){
                        if(message.getTo().equals(group.getId())){
                            return true;
                        }
                    }
                    return false;

                })
                .filter(message -> message.getDate().getMonth() == date.getMonth() && message.getDate().getYear() == date.getYear())
                .filter(message -> {
                    if(idFriend != 0){
                        return message.getFrom().equals(idFriend);
                    } else
                        return true;
                })
                .map(message -> {
                    String message1, firstName, lastName;
                    LocalDateTime date1 = message.getDate();
                    message1 = message.getMessage();
                    firstName = repoUsers.findOne(message.getFrom()).getFirstName();
                    lastName = repoUsers.findOne(message.getFrom()).getLastName();
                    return new MessageDTO(firstName, lastName, message1, date1);
                }).collect(Collectors.toList());
    }
}
