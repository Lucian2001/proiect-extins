package socialnetwork.service;

import socialnetwork.domain.ConversationDTO;
import socialnetwork.domain.Group;
import socialnetwork.domain.Message;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import socialnetwork.repository.database.AbstractDbRepository;
import socialnetwork.service.exceptions.ServiceException;

import java.awt.event.MouseWheelEvent;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class GroupsService {
    Repository<Long, Group> repo;
    Repository<Long, Utilizator> repuUsers;
    Repository<Long, Message> repoMessages;

    public GroupsService(Repository<Long, Group> repo, Repository<Long, Utilizator> repuUsers, Repository<Long, Message> repoMessages) {
        this.repo = repo;
        this.repuUsers = repuUsers;
        this.repoMessages = repoMessages;
    }

    public Group createGroup(List<Long> usersIdList){
        usersIdList.stream().forEach(idUser->{
            if(repuUsers.findOne(idUser) == null)
                throw new ServiceException("User negasit");
        });

        Group newGroup = new Group(usersIdList);
        repo.save(newGroup);
        return newGroup;
    }

    public void insertNewMessage(Group group, Message message){
            group.setLastMessage(message.getId());
            repo.update(group);
    }

    public Iterable<Group> getAll(){
        return repo.findAll();
    }

    public ConversationDTO getConversation(Long idGroup){
        Group group = repo.findOne(idGroup);
        ConversationDTO conversation = new ConversationDTO();
        if(group.getLastMessage() == null){

            return conversation;
        }
        Message message = repoMessages.findOne(group.getLastMessage());
        while (message != null) {
            conversation.addMessage(message);
            if(message.getReply() != null)
                message = repoMessages.findOne(message.getReply());
            else
                message = null;
        }
        return conversation;
    }

    public ConversationDTO getChatConversation(Long idUser1, Long idUser2){
        Optional<Group> group = StreamSupport.stream(repo.findAll().spliterator(), true)
                .filter(g->  (g.getUsers().size() == 2) && ( (g.getUsers().get(0).equals(idUser1) && g.getUsers().get(1).equals(idUser2))
                || (g.getUsers().get(1).equals(idUser1) && g.getUsers().get(0).equals(idUser2)))).findAny();
        if(group.isPresent()){
            ConversationDTO conversation = new ConversationDTO();
            if(group.get().getLastMessage() == null){
                return conversation;
            }
            Message message = repoMessages.findOne(group.get().getLastMessage());

            while (message != null) {
                conversation.addMessage(message);
                if(message.getReply() != null)
                    message = repoMessages.findOne(message.getReply());
                else
                    message = null;
            }
            return conversation;
        } else{
            List<Long> users = new ArrayList<>();
            users.add(idUser1);
            users.add(idUser2);
            createGroup(users);
            ConversationDTO conversation = new ConversationDTO();
            return conversation;
        }

    }
    public Group findOne(Long id){
        return repo.findOne(id);
    }

    public void addUserToGroup(Long idUser, Long idGroup){
        Group group = repo.findOne(idGroup);
        group.getUsers().add(idUser);
        repo.update(group);
    }



}
