package socialnetwork.service;

import socialnetwork.domain.SocialEvent;
import socialnetwork.domain.SocialEventNotification;
import socialnetwork.repository.Repository;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SocialEventNotificationService {
    Repository<Long, SocialEventNotification> repo;

    public SocialEventNotificationService(Repository<Long, SocialEventNotification> repo) {
        this.repo = repo;
    }

    void addSocialEventNotification(SocialEvent event, Long user){
        SocialEventNotification socialEventNotification = new SocialEventNotification(event.getDate(), user, event.getId(), event.getName());
        repo.save(socialEventNotification);
    }

    void deleteSocialEventNotification(SocialEvent event, Long user){
        repo.delete(StreamSupport.stream(repo.findAll().spliterator(), true)
                .filter(notification -> notification.getSocialEvent().equals(event.getId()) && notification.getUser().equals(user))
                .findFirst()
                .get().getId());
    }

    Boolean isSubscribedToEvent(SocialEvent event, Long user){
        return StreamSupport.stream(repo.findAll().spliterator(), true)
                .anyMatch(notification -> notification.getSocialEvent().equals(event.getId()) && notification.getUser().equals(user));
    }

    public List<SocialEventNotification> getAllNotificationsOfUser(Long user_id) {
        return StreamSupport.stream(repo.findAll().spliterator(), true)
                .filter(notification -> notification.getUser() == user_id)
                .collect(Collectors.toList());
    }
}
