package socialnetwork.service;

import javafx.scene.control.Alert;
import socialnetwork.controller.ScreenController;
import socialnetwork.domain.*;
import socialnetwork.repository.database.AbstractDbRepository;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.service.exceptions.ServiceException;
import socialnetwork.utils.*;
import sun.reflect.generics.scope.Scope;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class PageService implements Observable {
    private UtilizatorService userService;
    private FriendRequestService friendRequestService;
    private PrietenieService friendshipService;
    private GroupsService groupsService;
    private MessageService messageService;
    private SocialEventService socialEventService;
    private Page page = null;
    private SocialEventNotificationService socialEventNotificationService;
    private List<Observer> observers = new ArrayList<>();

    public PageService(UtilizatorService userService, FriendRequestService friendRequestService, PrietenieService friendshipService, GroupsService groupsService, MessageService messageService, SocialEventService socialEventService, SocialEventNotificationService socialEventNotificationService) {
        this.userService = userService;
        this.friendRequestService = friendRequestService;
        this.friendshipService = friendshipService;
        this.groupsService = groupsService;
        this.messageService = messageService;
        this.socialEventService = socialEventService;
        this.socialEventNotificationService = socialEventNotificationService;
    }

    @Override
    public void addObserver(Observer e) {
        observers.add(e);
    }

    @Override
    public void removeObserver(Observer e) {
        observers.remove(e);
    }

    @Override
    public void notifyObservers(Event<? extends EventObject> event) {
        observers.forEach(observer -> observer.update(event));
    }

    public  List<FriendRequestDTO> loadFriendRequestsReceived(List<FriendRequest> friendRequests, Long idUser){
        return friendRequests.stream()
                .filter(friendRequest -> friendRequest.getId().getRight().equals(idUser))
                .map(friendRequest -> new FriendRequestDTO(friendRequest.getId(),
                        userService.findOne(friendRequest.getId().getLeft()).getFirstName(),
                        userService.findOne(friendRequest.getId().getLeft()).getLastName(),
                        friendRequest.getStatus(),
                        friendRequest.getDate()))
                .collect(Collectors.toList());
    }

    public  List<FriendRequestDTO> loadFriendRequestsSent(List<FriendRequest> friendRequests, Long idUser) {
        return friendRequests.stream()
                .filter(friendRequest -> friendRequest.getId().getLeft().equals(idUser))
                .map(friendRequest -> new FriendRequestDTO(friendRequest.getId(),
                        userService.findOne(friendRequest.getId().getRight()).getFirstName(),
                        userService.findOne(friendRequest.getId().getRight()).getLastName(),
                        friendRequest.getStatus(),
                        friendRequest.getDate()))
                .collect(Collectors.toList());
    }

    public void init(Long idUser) throws ServiceException {
        Utilizator user = userService.findOne(idUser);
        List<FriendRequest> friendRequests = new ArrayList<>();
        friendRequests = friendRequestService.getAllFriendRequests(idUser).collect(Collectors.toList());

        page = new Page(idUser,
                user.getFirstName(),
                user.getLastName(),
                friendshipService.getUserFriends(idUser),
                loadFriendRequestsSent(friendRequests, idUser),
                loadFriendRequestsReceived(friendRequests, idUser));



    }

    public void showNotifications(){
        MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Cerere de prietenie!", "dsadsa");
    }

    public List<FriendDTO> getFriends(int pageNr, int pageSize){
        page.setFriends(friendshipService.getUserFriends(page.getIdUser()));
        return page.getFriends().stream()
             .skip(pageNr  * pageSize)
                .limit(pageSize)
                .collect(Collectors.toList());
    }

    public List<Utilizator> getAllNonFriends(int pageNr, int pageSize){
        return userService.getAllNonFriends(new Pageable(pageNr, pageSize), page.getIdUser()).getContent()
                .collect(Collectors.toList());
    }


    public List<FriendRequestDTO> getFriendRequestsSent(){
        return page.getFriendRequestsSent();
    }

    public List<FriendRequestDTO> getFriendRequestsReceived(){
        return page.getFriendRequestsReceived();
    }

    public void solveFriendRequest(Long userId, Integer accepted){
        try {
            friendRequestService.solveFriendRequest(page.getIdUser(), userId, accepted);
        } catch (ServiceException ex){
            MessageAlert.showMessage(null, Alert.AlertType.ERROR, "Cerere de prietenie!", ex.getMessage());
        }

        List<FriendRequestDTO> friendRequestsReceived = new ArrayList<>();
        page.setFriendRequestsReceived(loadFriendRequestsReceived(friendRequestService.getAllFriendRequests(page.getIdUser()).collect(Collectors.toList()), page.getIdUser()));
        if(accepted == 1) {
            notifyObservers(new Event<>(new CommonEvent(EventType.FRIEND_REQUEST_ACCEPT)));

        }else
            notifyObservers(new Event<>(new CommonEvent(EventType.FRIEND_REQUEST_REJECT)));
    }

    public void sendFriendRequest(Long id){
        if(friendRequestService.existFriendRequest(page.getIdUser(), id)) {
            friendRequestService.addFriendRequest(page.getIdUser(), id);
            page.setFriendRequestsSent(loadFriendRequestsSent(friendRequestService.getAllFriendRequests(page.getIdUser()).collect(Collectors.toList()), page.getIdUser()));
            notifyObservers(new Event<>(new CommonEvent(EventType.SEND_FRIEND_REQUEST)));
        } else{
            MessageAlert.showMessage(null, Alert.AlertType.INFORMATION, "Cerere de prietenie", "Exista deja o cerere de prietenie!");

        }
    }

    public void deleteFriend(Long id){
        try {
            friendshipService.removeFriendship(page.getIdUser(), id);
        } catch(ServiceException ex){
            MessageAlert.showMessage(null, Alert.AlertType.ERROR, "Stergere prieten!", ex.getMessage());
        }
        page.setFriendRequestsSent(loadFriendRequestsSent(friendRequestService.getAllFriendRequests(page.getIdUser()).collect(Collectors.toList()), page.getIdUser()));
        page.setFriendRequestsReceived(loadFriendRequestsReceived(friendRequestService.getAllFriendRequests(page.getIdUser()).collect(Collectors.toList()), page.getIdUser()));
        notifyObservers(new Event<>(new CommonEvent(EventType.DELETE_FRIEND)));
    }

    public void retractFriendRequest(Long id){
        try {
            friendRequestService.deleteFriendRequest(page.getIdUser(), id);
        } catch (ServiceException ex){
            MessageAlert.showMessage(null, Alert.AlertType.ERROR, "Retrage cererea de prietenie!", ex.getMessage());

        }

        page.setFriendRequestsSent(loadFriendRequestsSent(friendRequestService.getAllFriendRequests(page.getIdUser()).collect(Collectors.toList()), page.getIdUser()));
        notifyObservers(new Event<>(new CommonEvent(EventType.RETRACT_FRIEND_REQUEST)));
    }

    public void openChat(Long id){
        page.addConversation(id, groupsService.getChatConversation(page.getIdUser(), id));
        notifyObservers(new Event<>(new OpenChatEvent(EventType.OPEN_CHAT,  userService.findOne(id).getFirstName() + " " + userService.findOne(id).getLastName(), id)));
    }

    public Message sendMessage(Long id, String message){
        Message msg = messageService.sendChatMessage(page.getIdUser(), id, message);
        page.addMessageToConversation(id, msg);
        if (msg != null)
            return msg;
        else return null;
    }

    public ConversationDTO getConversation(Long id){
        return page.getConversation(id);
    }

    public ConversationDTO getGroupConversation(Long idGroup){
        return groupsService.getConversation(idGroup);
    }

    public Utilizator getUser(Long id){
        return userService.findOne(id);
    }


    public void sendMessageGroup(Long idGroup, String message){
        messageService.sendMessage(page.getIdUser(), idGroup, message);
    }

    public void addEvent(String name, String description, String image, LocalDateTime date) {
        socialEventService.createSocialEvent(page.getIdUser(), name, description, image, date);

    }

    public List<SocialEvent> getUserSocialEvents(){
        return socialEventService.getUserSocialEvents(page.getIdUser());
    }

    public List<SocialEvent> getSocialEvents(){
        return socialEventService.getSocialEvents(page.getIdUser());
    }

    public void getDefaultSocialEvent(){
        if(socialEventService.getUserSocialEvents(page.getIdUser()).size() > 0)
            notifyObservers(new Event<>(new OpenSocialEventEvent(EventType.OPEN_EVENT, socialEventService.getUserSocialEvents(page.getIdUser()).get(0), true)));
    }

    public void joinSocialEvent(SocialEvent event){
        groupsService.addUserToGroup(page.getIdUser(), event.getGroup());
        notifyObservers(new Event<>(new CommonEvent(EventType.JOIN_EVENT)));
    }

    public void openSocialEvent(SocialEvent socialEvent, Boolean isPartOf){
        notifyObservers(new Event<>(new OpenSocialEventEvent(EventType.OPEN_EVENT, socialEvent, isPartOf)));
    }

    public void addNotification(SocialEvent event){
        socialEventNotificationService.addSocialEventNotification(event, page.getIdUser());
    }


    public Boolean isSubscribed(SocialEvent event) {
        return socialEventNotificationService.isSubscribedToEvent(event, page.getIdUser());
    }

    public void deleteNotification(SocialEvent currentSocialEvent) {
        socialEventNotificationService.deleteSocialEventNotification(currentSocialEvent, page.getIdUser());
    }

    public List<SocialEventNotification> getNotifications() {
        return socialEventNotificationService.getAllNotificationsOfUser(page.getIdUser());
    }

    public void logOut()  {
        page = null;
        try {
            ScreenController.get_instance().activate("loginScreen");
        }
        catch (Exception x){

        }
    }
}


