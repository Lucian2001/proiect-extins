package socialnetwork.service;

import socialnetwork.domain.Entity;
import socialnetwork.domain.Utilizator;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class DisjointUnionSets<E extends Entity<Long>> {
    HashMap<Long, Set<E>> sets;
    HashMap<Long, Long> parent;
    HashMap<Long, HashMap<Long, HashMap<Long, Integer>>> paths;
    Integer maxPath = 0;

    DisjointUnionSets(){
        sets = new HashMap<>();
        parent = new HashMap<>();
        paths = new HashMap<>();
    }

    void add(E elem){
        sets.put(elem.getId(), new HashSet<E>());
        sets.get(elem.getId()).add(elem);
        parent.put(elem.getId(), elem.getId());
        Integer x = 1;
        paths.put(elem.getId(), new HashMap<>());
        paths.get(elem.getId()).put(elem.getId(), new HashMap<>());
    }
    void calculatePath(E elem1, E elem2){
        HashMap<Long, HashMap<Long, Integer>> matrix = paths.get(parent.get(elem1.getId()));
        Long nodExistent, nodDeAdaugat;
        if(matrix.get(elem1.getId()) != null) {
            nodExistent = elem1.getId();
            nodDeAdaugat = elem2.getId();
        } else{
            nodExistent = elem2.getId();
            nodDeAdaugat = elem1.getId();
        }
        if(matrix.get(nodDeAdaugat) == null) {
            matrix.put(nodDeAdaugat, new HashMap<>());
            matrix.get(nodExistent).put(nodDeAdaugat, 1);
            matrix.get(nodDeAdaugat).put(nodExistent, 1);
            for (Map.Entry<Long, HashMap<Long, Integer>> x : matrix.entrySet()) {
                if (!x.getKey().equals(nodExistent) && !x.getKey().equals(nodDeAdaugat)) {
                    x.getValue().put(nodDeAdaugat, x.getValue().get(nodExistent) + 1);
                    matrix.get(nodDeAdaugat).put(x.getKey(), x.getValue().get(nodExistent) + 1);
                    if(x.getValue().get(nodExistent) + 1 > maxPath){
                        maxPath = x.getValue().get(nodExistent) + 1;
                    }
                }
            }
            paths.remove(nodDeAdaugat);
        } else{
            for (Map.Entry<Long, Integer> x : matrix.get(nodDeAdaugat).entrySet()) {
               if(matrix.get(nodExistent).get(x.getKey()) > x.getValue() && !x.getKey().equals(nodDeAdaugat)){
                   x.setValue(matrix.get(nodExistent).get(x.getKey()));
               }
            }

            for (Map.Entry<Long, Integer> x : matrix.get(nodExistent).entrySet()) {
                if(matrix.get(nodDeAdaugat).get(x.getKey()) > x.getValue()){
                    x.setValue(matrix.get(nodDeAdaugat).get(x.getKey()));
                }
            }
        }
    }

    void calculatePathBig(E elem1, E elem2){
        HashMap<Long, HashMap<Long, Integer>> matrix1 = paths.get(parent.get(elem1.getId()));
        HashMap<Long, HashMap<Long, Integer>> matrix2 = paths.get(parent.get(elem2.getId()));

        matrix2.get(elem2.getId()).put(elem1.getId(), 1);
        matrix1.get(elem1.getId()).put(elem2.getId(), 1);
        for (Map.Entry<Long, HashMap<Long, Integer>> x2 : matrix2.entrySet()) {
            if(!x2.getKey().equals(elem2.getId())) {
                matrix2.get(x2.getKey()).put(elem1.getId(), matrix2.get(elem2.getId()).get(x2.getKey()) + 1);
                matrix1.get(elem1.getId()).put(x2.getKey(), matrix2.get(elem2.getId()).get(x2.getKey()) + 1);
            }
        }
        for (Map.Entry<Long, HashMap<Long, Integer>> x1 : matrix1.entrySet()) {
            if(!x1.getKey().equals(elem1.getId())) {
                matrix1.get(x1.getKey()).put(elem2.getId(), matrix1.get(elem1.getId()).get(x1.getKey()) + 1);
                matrix2.get(elem2.getId()).put(x1.getKey(), matrix1.get(elem1.getId()).get(x1.getKey()) + 1);
            }
        }


        for (Map.Entry<Long, HashMap<Long, Integer>> x2 : matrix2.entrySet()) {
            for (Map.Entry<Long, HashMap<Long, Integer>> x1 : matrix1.entrySet()) {
                if (!x1.getKey().equals(elem1.getId()) && !x2.getKey().equals(elem2.getId())) {
                    matrix2.get(x2.getKey()).put(x1.getKey(), matrix2.get(elem2.getId()).get(x2.getKey()) + 1 + matrix1.get(elem1.getId()).get(x1.getKey()));
                    matrix1.get(x1.getKey()).put(x2.getKey(), matrix2.get(elem2.getId()).get(x2.getKey()) + 1 + matrix1.get(elem1.getId()).get(x1.getKey()));
                    if(matrix2.get(elem2.getId()).get(x2.getKey()) + 1 + matrix1.get(elem1.getId()).get(x1.getKey()) > maxPath){
                        maxPath = matrix2.get(elem2.getId()).get(x2.getKey()) + 1 + matrix1.get(elem1.getId()).get(x1.getKey());
                    }
                }
            }
        }

        for (Map.Entry<Long, HashMap<Long, Integer>> x : matrix2.entrySet()) {
            matrix1.put(x.getKey(), x.getValue());
        }
        paths.remove(parent.get(elem2.getId()));
    }


    void addConnection(E elem1, E elem2){
        Set<E> set1 = sets.get(elem1.getId());
        if(set1 == null){
            set1 = sets.get(parent.get(elem1.getId()));
        }
        Set<E> set2 = sets.get(elem2.getId());
        if(set2 == null){
            set2 = sets.get(parent.get(elem2.getId()));
        }
        if(set1 == set2){
            calculatePath(elem1, elem2);
        } else if(set1.size() > set2.size()){
            if(set2.size() == 1)
                calculatePath(elem1, elem2);
            else
                calculatePathBig(elem1, elem2);
            set1.addAll(set2);
            sets.remove(parent.get(elem2.getId()));

            parent.put(elem2.getId(), elem1.getId());

        } else{
            if(set1.size() == 1)
                calculatePath(elem2, elem1);
            else
                calculatePathBig(elem2, elem1);
            set2.addAll(set1);
            sets.remove(parent.get(elem1.getId()));
            parent.put(elem1.getId(), parent.get(elem2.getId()));

        }
    }

    public Integer numberOfSets(){
        return sets.size();
    }

    public Integer getMaxPath(){
        return maxPath;
    }

}
