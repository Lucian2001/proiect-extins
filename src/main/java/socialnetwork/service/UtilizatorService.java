package socialnetwork.service;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.FilterFunction;
import socialnetwork.repository.paging.Page;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.repository.paging.PagingRepository;
import socialnetwork.service.exceptions.ServiceException;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import javax.security.auth.login.LoginException;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class UtilizatorService {
    private PagingRepository<Long, Utilizator> repo;
    private Repository<Tuple<Long, Long>, Prietenie> repoFriendships;
    private ComunityService comunityService;
    Long currentEmptyId = 0L;

    /**
     * Clasa gestioneaza lucrul cu useri
     * @param repo -repository cu useri
     * @param comunityService - service care gestioneaza comunitatile
     */
    public UtilizatorService(PagingRepository<Long, Utilizator> repo,Repository<Tuple<Long, Long>, Prietenie> repoFriendships, ComunityService comunityService) {
        this.repo = repo;
        this.repoFriendships = repoFriendships;
        if (repo.getId() != null)
            currentEmptyId = repo.getId() + 1;
        this.comunityService = comunityService;

    }

    /**
     * Functia adauga un user
     * @param name prenumele unui user
     * @param lastName numele de familie al unui user
     * @throws ValidationException daca datale sunt invalide
     */
    public void addUser(String name, String lastName, String mail, String password) throws ValidationException {
        password = hashPassword(password);
        if(password == null){
            throw new ServiceException("C problema de hash!");
        }
        Utilizator user = new Utilizator(name, lastName, mail, password);

        repo.save(user);
        comunityService.addUser(user);
    }

    /**
     * Se sterge un user
     * @param id id-ul user-ului care trebuie sters
     * @throws ServiceException daca nu se gaseste utilizatorul dat
     * */
    public void removeUser(Long id) throws ServiceException {
        Utilizator response = repo.delete(id);
        if (response == null){
            throw new ServiceException("Utilizator inexistent!");
        }
        Iterable<Prietenie> friendships = repoFriendships.findAll();
        List<Tuple<Long, Long>> deSters = StreamSupport.stream(friendships.spliterator(), false)
                .filter(friendship -> friendship.getId().getRight() == id || friendship.getId().getLeft() == id)
                .map(Prietenie::getId)
                .collect(Collectors.toList());
        for (Tuple<Long, Long> x:deSters){
            repoFriendships.delete(x);
        }


    }
    public Iterable<Utilizator> getAll(){
        return repo.findAll();
    }

    public boolean userExist(Long id){
        return repo.findOne(id) != null;
    }

    public Page<Utilizator> getAllNonFriends(Pageable pageable, Long idUser){
        return repo.findAll(pageable, idUser);
    }

    public Utilizator findOne(Long id) throws ServiceException{
        Utilizator user = repo.findOne(id);
        if (user == null)
            throw new ServiceException("Userul nu exista!");
        return user;
    }

    public Long login(String mail, String password){
        Iterable<Utilizator> users = repo.findAll();
        for(Utilizator user:users){
            if (user.getMail()!= null && user.getMail().equals(mail)){
                if(user.getPassword().equals(hashPassword(password)))
                    return  user.getId();
            }
        }
        throw new ServiceException("Credentiale incorecte!");
    }

    private String hashPassword(String password){
        try {

            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method is called to calculate message digest
            //  of an input digest() return array of byte
            byte[] messageDigest = md.digest(password.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        }
        // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }


    }




}
