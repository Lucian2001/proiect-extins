package socialnetwork.service;

import socialnetwork.domain.*;
import socialnetwork.domain.validators.ValidationException;
import socialnetwork.repository.Repository;
import socialnetwork.repository.paging.Pageable;
import socialnetwork.service.exceptions.ServiceException;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class PrietenieService {

    private Repository<Tuple<Long, Long>, Prietenie> repo;
    private Repository<Long, Utilizator> repoUsers;
    private Repository<Tuple<Long, Long>, FriendRequest> repoFriendRequests;
    private ComunityService comunityService;
    private GroupsService groupsService;

    /**
     * Clasa care se ocupa de gestionarea prieteniilor
     * @param repo - referinta la un repository de prietenii
     * @param repoUsers - referinta la un repository de useri
     * @param comunityService - referinta la service-ul de comunitati
     */
    public PrietenieService(Repository<Tuple<Long, Long>, Prietenie> repo, Repository<Long, Utilizator> repoUsers , ComunityService comunityService,Repository<Tuple<Long, Long>, FriendRequest> repoFriendRequests, GroupsService groupsService) {
        this.repo = repo;
        this.repoUsers = repoUsers;
        this.comunityService = comunityService;
        this.repoFriendRequests = repoFriendRequests;
        this.groupsService = groupsService;
        //loadFriendships();
    }

    /**
     * Functia incarcarca toate prieteniile la deschiderea programului
     */
    private void loadFriendships(){
        Iterable<Prietenie> friendships = repo.findAll();
        for(Prietenie friendship:friendships){
            Utilizator user1 = repoUsers.findOne(friendship.getId().getLeft());
            Utilizator user2 = repoUsers.findOne(friendship.getId().getRight());
            user1.addFriend(user2);
            user2.addFriend(user1);
            comunityService.addFriendship(user1, user2);
        }
    }

    /**
     * Functia adauga o prietenie
     * @param id1
     * @param id2
     * @throws ServiceException daca utilizatorul este inexistent
     * @throws ValidationException daca prietenia este invalida
     */
    public void addFriendship(Long id1, Long id2) throws ServiceException, ValidationException {
        if (id2 < id1){
            Long x = id2;
            id2 = id1;
            id1 = x;
        }
        Tuple<Long, Long> id = new Tuple<>(id1, id2);
        Prietenie friendship = new Prietenie();
        friendship.setId(id);

        Utilizator user1 = repoUsers.findOne(id1);
        if(user1 == null){
            throw new ServiceException("Utilizatorul" + id1.toString() +  "este inexistent!");
        }
        Utilizator user2 = repoUsers.findOne(id2);
        if(user2 == null){
            throw new ServiceException("Utilizatorul " + id2.toString() +  " este inexistent!");
        }

        user1.addFriend(user2);
        user2.addFriend(user1);
        repo.save(friendship);
        comunityService.addFriendship(user1, user2);
    }

    /**
     * Functia sterge o prietenie
     * @param id1
     * @param id2
     * @throws ServiceException daca utilizatorul este inexistent
     * @throws ValidationException daca prietenia este invalida
     */
    public void removeFriendship(Long id1, Long id2) throws ServiceException,ValidationException {
        if (id2 < id1){
            Long x = id2;
            id2 = id1;
            id1 = x;
        }
        Tuple<Long, Long> id = new Tuple<Long, Long>(id1, id2);

        Utilizator user1 = repoUsers.findOne(id1);
        if(user1 == null){
            throw new ServiceException("Utilizatorul 1 este inexistent!");
        }
        Utilizator user2 = repoUsers.findOne(id2);
        if(user2 == null){
            throw new ServiceException("Utilizatorul 2 este inexistent!");
        }
        Prietenie prietenie = repo.findOne(id);
        if (prietenie == null){
            throw new ServiceException("Prietenia este inexistenta!");
        }
        user1.removeFriend(user2);
        user2.removeFriend(user1);
        repo.delete(id);
        if(repoFriendRequests.findOne(id) != null){
            repoFriendRequests.delete(id);
        } else {
            repoFriendRequests.delete(new Tuple<>(id2, id1));
        }
    }

    /**
     * Functia returneza toate prieteniile
     * @return Iterable
     */
    public Iterable<Prietenie> getAll(){
        return repo.findAll();
    }


    public List<FriendDTO> getUserFriends(Long id){
        Iterable<Prietenie> friendships = repo.findAll();
        return StreamSupport.stream(friendships.spliterator(), false)
                .filter(friendship -> (friendship.getId().getLeft() == id || friendship.getId().getRight() == id))
                .map((friendship) ->{
                    String firstName, lastName;
                    LocalDateTime date = friendship.getDate();
                    FriendDTO x= null;
                    if(friendship.getId().getLeft() == id) {
                        firstName = repoUsers.findOne(friendship.getId().getRight()).getFirstName();
                        lastName = repoUsers.findOne(friendship.getId().getRight()).getLastName();
                        x = new FriendDTO(firstName, lastName, date);
                        x.setId(friendship.getId().getRight());
                    } else{
                        firstName = repoUsers.findOne(friendship.getId().getLeft()).getFirstName();
                        lastName = repoUsers.findOne(friendship.getId().getLeft()).getLastName();
                        x = new FriendDTO(firstName, lastName, date);
                        x.setId(friendship.getId().getLeft());
                    }
                    return x;
                })
                .collect(Collectors.toList());
    }

    public List<FriendDTO> getUserFriendsOnSpecificDate(Long id, Integer month, Integer year){
        LocalDate date = LocalDate.of(year, month, 1);
        Iterable<Prietenie> friendships = repo.findAll();
        return StreamSupport.stream(friendships.spliterator(), false)
                .filter(friendship -> (friendship.getId().getLeft() == id || friendship.getId().getRight() == id))
                .filter(friendship -> friendship.getDate().getMonth() == date.getMonth() && friendship.getDate().getYear() == date.getYear())
                .map((friendship) ->{
                    String firstName, lastName;
                    LocalDateTime date1 = friendship.getDate();
                    if(friendship.getId().getLeft() == id) {
                        firstName = repoUsers.findOne(friendship.getId().getRight()).getFirstName();
                        lastName = repoUsers.findOne(friendship.getId().getRight()).getLastName();
                    } else{
                        firstName = repoUsers.findOne(friendship.getId().getLeft()).getFirstName();
                        lastName = repoUsers.findOne(friendship.getId().getLeft()).getLastName();
                    }
                    return new FriendDTO(firstName, lastName, date1);
                }).collect(Collectors.toList());
    }


}
