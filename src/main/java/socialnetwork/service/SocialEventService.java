package socialnetwork.service;

import socialnetwork.domain.Group;
import socialnetwork.domain.SocialEvent;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

public class SocialEventService {
    Repository<Long, SocialEvent> repo;
    GroupsService groupsService;

    public SocialEventService(Repository<Long, SocialEvent> repo, GroupsService groupsService) {
        this.repo = repo;
        this.groupsService = groupsService;
    }

    public void createSocialEvent(Long idCreator, String name, String description, String image, LocalDateTime date){
        List<Long> userIdList = new ArrayList<>();
        userIdList.add(idCreator);
        Group group = groupsService.createGroup(userIdList);
        SocialEvent socialEvent = new SocialEvent(name, image, description, idCreator, group.getId(), date);
        repo.save(socialEvent);
    }

    private boolean isInGroup(Long id, Long idGroup){
        Group group =  groupsService.findOne(idGroup);
        for(Long user: group.getUsers()){
            if(user.equals(id)){
                return true;
            }
        }
        return false;
    }
    List<SocialEvent> getUserSocialEvents(Long id){

        return StreamSupport.stream(repo.findAll().spliterator(), true)
                .filter(event -> isInGroup(id, event.getGroup()))
                .collect(Collectors.toList());
    }

    List<SocialEvent> getSocialEvents(Long id){

        return StreamSupport.stream(repo.findAll().spliterator(), true)
                .filter(event -> !isInGroup(id, event.getGroup()))
                .collect(Collectors.toList());
    }

}
