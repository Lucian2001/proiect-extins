package socialnetwork.service;

import socialnetwork.domain.Prietenie;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

public class ComunityService {
    private Repository<Long, Utilizator> repo;
    private DisjointUnionSets<Utilizator> disjointSet;

    public ComunityService(Repository<Long, Utilizator> repo) {
        this.repo = repo;
        disjointSet = new DisjointUnionSets<>();
        //init();
    }

    private void init(){
        Iterable<Utilizator> users = repo.findAll();
        for(Utilizator user:users){
            this.addUser(user);
        }
        Integer x;
    }

    /**
     * Cauta drumul maxim intr-o componenta conexa
     * @param visited set de noduri vizitate
     * @param user
     * @param max se schimba valoarea asta
     */
    private void DFSLongestPath(Set<Long> visited, Utilizator user, AtomicInteger max){
        visited.add(user.getId());
        List<Utilizator> friends = user.getFriends();
        int ok =0;
        for(Utilizator friend: friends) {

            if(!visited.contains(friend.getId())){
                ok = 1;
                Set<Long> x = new TreeSet<Long>(visited);
                DFSLongestPath(x, friend, max);
            }
        }
        if(ok == 0) {
            if(visited.size() > max.get()){
                max.set(visited.size());
            }
        }
    }

    /**
     *  Face Dfs pe un user si completeaza set-ul visited cu nodurile pe care le viziteaza
     * @param visited
     * @param user
     */
    private void DFS(Set<Long> visited, Utilizator user){
        visited.add(user.getId());
        List<Utilizator> friends = user.getFriends();
        for(Utilizator friend: friends) {
            if(!visited.contains(friend.getId())){
                DFS(visited, friend);
            }
        }
    }

    /**
     * calculeaza numarul de comunitati
     * @return Integer
     */
    public Integer getNumberOfComunities(){
        Iterable<Utilizator> users = repo.findAll();
        Set<Long> visited = new TreeSet<Long>();
        Integer nrOfComunities = 0;
        for(Utilizator user:users){
            if(!visited.contains(user.getId())){
                DFS(visited, user);
                nrOfComunities++;
            }
        }
        return nrOfComunities;
    }


    public ArrayList<Utilizator> getMostSociableComunity() {
        Iterable<Utilizator> users = repo.findAll();

        Integer nrOfComunities = 0;
        AtomicInteger max = new AtomicInteger();
        Integer maxPath = -1;
        Utilizator userMax = null;
        for (Utilizator user : users) {
            Set<Long> visited = new TreeSet<Long>();
            max.set(-1);
            DFSLongestPath(visited, user, max);
            if(max.get() > maxPath){
                maxPath = max.get();
                userMax = user;
            }
        }
        System.out.println(maxPath);
        ArrayList<Utilizator> usersResult = new ArrayList<>();
        Set<Long> visited = new TreeSet<Long>();
        DFS(visited, userMax);
        for(Long x:visited){
            usersResult.add(repo.findOne(x));
        }

       return usersResult;
    }


    public void addUser(Utilizator user){
        //disjointSet.add(user);
    }

    public void addFriendship(Utilizator user1, Utilizator user2){
       // disjointSet.addConnection(user1, user2);
    }

}
