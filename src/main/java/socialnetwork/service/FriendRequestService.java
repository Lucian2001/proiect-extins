package socialnetwork.service;

import com.sun.org.apache.xpath.internal.operations.Bool;
import socialnetwork.domain.FriendRequest;
import socialnetwork.domain.FriendRequestStatus;
import socialnetwork.domain.Tuple;
import socialnetwork.domain.Utilizator;
import socialnetwork.repository.Repository;
import socialnetwork.service.exceptions.ServiceException;

import java.util.List;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

public class FriendRequestService {
    Repository<Tuple<Long, Long>, FriendRequest> repo;
    Repository<Long, Utilizator> repoUsers;
    PrietenieService friendshipService;


    public FriendRequestService(Repository<Tuple<Long, Long>, FriendRequest> repo, Repository<Long, Utilizator> repoUsers, PrietenieService friendshipService) {
        this.repo = repo;
        this.repoUsers = repoUsers;
        this.friendshipService = friendshipService;
    }

    public void addFriendRequest(Long idUser, Long id) throws ServiceException{
        Utilizator user = repoUsers.findOne(id);
        if (user == null)
            throw new ServiceException("Utilizatorul nu exista!");
        FriendRequest friendRequest = new FriendRequest();
        friendRequest.setId(new Tuple<>(idUser, id));
        repo.save(friendRequest);
    }

    public Stream<FriendRequest> getAllFriendRequests(Long idUser){
        return StreamSupport.stream(repo.findAll().spliterator(), false)
                .filter(friendRequest -> friendRequest.getId().getLeft().equals(idUser)
        || friendRequest.getId().getRight().equals(idUser));
    }

    public void solveFriendRequest(Long idUser, Long id, Integer accepted) throws ServiceException{
        FriendRequest friendRequest = repo.findOne(new Tuple<>(id, idUser));
        if(friendRequest == null ){
            throw new ServiceException("Aceasta cerere de prietenie nu exista!");
        }
        if(friendRequest.getStatus() != FriendRequestStatus.pending){
            throw new ServiceException("Aceasta cerere a fost rezolvata!");
        }
        if(accepted == 1) {
            friendRequest.setStatus(FriendRequestStatus.approved);
            friendshipService.addFriendship(idUser, id);
        } else{
            friendRequest.setStatus(FriendRequestStatus.rejected);
        }
        repo.update(friendRequest);
    }

    public void deleteFriendRequest(Long idUser, Long id) throws ServiceException{
        FriendRequest friendRequest = repo.findOne(new Tuple<>(idUser, id));
        if(friendRequest == null ){
            throw new ServiceException("Aceasta cerere de prietenie nu exista!");
        }
        if(friendRequest.getStatus() != FriendRequestStatus.pending){
            throw new ServiceException("Aceasta cerere de prietenia a fost deja rezolvata!");
        }
        repo.delete(friendRequest.getId());
    }

    public boolean existFriendRequest(Long idUser, Long idFriend){
        FriendRequest friendRequest = repo.findOne(new Tuple<>(idUser, idFriend));
        if(friendRequest == null ) {
            return true;
        }
        FriendRequest friendRequest1 = repo.findOne(new Tuple<>(idFriend, idUser));
        if(friendRequest1 == null){
            return true;
        }
        return false;
    }

}
