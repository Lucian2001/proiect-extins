package socialnetwork.exceptions;

public class BadDateException extends RuntimeException{
    public BadDateException(String message) {
        super(message);
    }

}
