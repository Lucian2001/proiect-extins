PGDMP         4                 y            socialnetwork    13.0    13.0 E               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false                       1262    16394    socialnetwork    DATABASE     l   CREATE DATABASE socialnetwork WITH TEMPLATE = template0 ENCODING = 'UTF8' LOCALE = 'Romanian_Romania.1250';
    DROP DATABASE socialnetwork;
                postgres    false                        2615    57358    database    SCHEMA        CREATE SCHEMA database;
    DROP SCHEMA database;
                postgres    false            �            1259    24588    users    TABLE     �   CREATE TABLE public.users (
    id integer NOT NULL,
    first_name character varying(100) NOT NULL,
    last_name character varying(100) NOT NULL,
    mail character varying(100),
    password character varying(200)
);
    DROP TABLE public.users;
       public         heap    postgres    false            �            1259    24586    Users_id_seq    SEQUENCE     �   CREATE SEQUENCE public."Users_id_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 %   DROP SEQUENCE public."Users_id_seq";
       public          postgres    false    202                       0    0    Users_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public."Users_id_seq" OWNED BY public.users.id;
          public          postgres    false    201            �            1259    57359    events    TABLE     �   CREATE TABLE public.events (
    id integer NOT NULL,
    name character varying(50),
    description character varying(1000),
    group_id integer,
    image character varying(100),
    creator integer,
    date timestamp without time zone
);
    DROP TABLE public.events;
       public         heap    postgres    false            �            1259    57362    events_id_seq    SEQUENCE     �   CREATE SEQUENCE public.events_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.events_id_seq;
       public          postgres    false    211                       0    0    events_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.events_id_seq OWNED BY public.events.id;
          public          postgres    false    212            �            1259    57364    events_notifications    TABLE     q   CREATE TABLE public.events_notifications (
    id integer NOT NULL,
    event_id integer,
    user_id integer
);
 (   DROP TABLE public.events_notifications;
       public         heap    postgres    false            �            1259    57367    events_notifications_id_seq    SEQUENCE     �   CREATE SEQUENCE public.events_notifications_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.events_notifications_id_seq;
       public          postgres    false    213                       0    0    events_notifications_id_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.events_notifications_id_seq OWNED BY public.events_notifications.id;
          public          postgres    false    214            �            1259    32778    friend_requests    TABLE     �   CREATE TABLE public.friend_requests (
    user_from_id integer NOT NULL,
    user_to_id integer NOT NULL,
    status integer DEFAULT 0,
    date timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);
 #   DROP TABLE public.friend_requests;
       public         heap    postgres    false            �            1259    24597    friendships    TABLE     q   CREATE TABLE public.friendships (
    user1_id integer NOT NULL,
    user2_id integer NOT NULL,
    date date
);
    DROP TABLE public.friendships;
       public         heap    postgres    false            �            1259    57369    friendships_id_seq    SEQUENCE     �   CREATE SEQUENCE public.friendships_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 )   DROP SEQUENCE public.friendships_id_seq;
       public          postgres    false            �            1259    32804    groups    TABLE     R   CREATE TABLE public.groups (
    id integer NOT NULL,
    last_message integer
);
    DROP TABLE public.groups;
       public         heap    postgres    false            �            1259    32802    groups_id_seq    SEQUENCE     �   ALTER TABLE public.groups ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.groups_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    208            �            1259    32839    groups_users    TABLE     i   CREATE TABLE public.groups_users (
    id integer NOT NULL,
    user_id integer,
    group_id integer
);
     DROP TABLE public.groups_users;
       public         heap    postgres    false            �            1259    32837    groups_users_id_seq    SEQUENCE     �   ALTER TABLE public.groups_users ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.groups_users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    210            �            1259    32784    messages    TABLE     �   CREATE TABLE public.messages (
    "time" timestamp without time zone DEFAULT now() NOT NULL,
    id integer NOT NULL,
    text character varying(500),
    reply_to integer,
    user_from integer,
    group_id integer
);
    DROP TABLE public.messages;
       public         heap    postgres    false            �            1259    32782    messages_id_seq    SEQUENCE     �   ALTER TABLE public.messages ALTER COLUMN id ADD GENERATED BY DEFAULT AS IDENTITY (
    SEQUENCE NAME public.messages_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1
);
            public          postgres    false    206            �            1259    57371    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public          postgres    false    202                       0    0    users_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.users_id_seq OWNED BY public.users.id;
          public          postgres    false    216            T           2604    57373 	   events id    DEFAULT     f   ALTER TABLE ONLY public.events ALTER COLUMN id SET DEFAULT nextval('public.events_id_seq'::regclass);
 8   ALTER TABLE public.events ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    212    211            U           2604    57374    events_notifications id    DEFAULT     �   ALTER TABLE ONLY public.events_notifications ALTER COLUMN id SET DEFAULT nextval('public.events_notifications_id_seq'::regclass);
 F   ALTER TABLE public.events_notifications ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    214    213            P           2604    57375    users id    DEFAULT     d   ALTER TABLE ONLY public.users ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 7   ALTER TABLE public.users ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    216    202                      0    57359    events 
   TABLE DATA           W   COPY public.events (id, name, description, group_id, image, creator, date) FROM stdin;
    public          postgres    false    211   �Q                 0    57364    events_notifications 
   TABLE DATA           E   COPY public.events_notifications (id, event_id, user_id) FROM stdin;
    public          postgres    false    213   �X       �          0    32778    friend_requests 
   TABLE DATA           Q   COPY public.friend_requests (user_from_id, user_to_id, status, date) FROM stdin;
    public          postgres    false    204   Y       �          0    24597    friendships 
   TABLE DATA           ?   COPY public.friendships (user1_id, user2_id, date) FROM stdin;
    public          postgres    false    203   �Y                  0    32804    groups 
   TABLE DATA           2   COPY public.groups (id, last_message) FROM stdin;
    public          postgres    false    208   Z                 0    32839    groups_users 
   TABLE DATA           =   COPY public.groups_users (id, user_id, group_id) FROM stdin;
    public          postgres    false    210   WZ       �          0    32784    messages 
   TABLE DATA           S   COPY public.messages ("time", id, text, reply_to, user_from, group_id) FROM stdin;
    public          postgres    false    206   �Z       �          0    24588    users 
   TABLE DATA           J   COPY public.users (id, first_name, last_name, mail, password) FROM stdin;
    public          postgres    false    202   ^                  0    0    Users_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public."Users_id_seq"', 1, false);
          public          postgres    false    201                       0    0    events_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.events_id_seq', 5, true);
          public          postgres    false    212                       0    0    events_notifications_id_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.events_notifications_id_seq', 44, true);
          public          postgres    false    214                       0    0    friendships_id_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.friendships_id_seq', 7, true);
          public          postgres    false    215                       0    0    groups_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.groups_id_seq', 16, true);
          public          postgres    false    207                       0    0    groups_users_id_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.groups_users_id_seq', 146, true);
          public          postgres    false    209                       0    0    messages_id_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.messages_id_seq', 41, true);
          public          postgres    false    205                       0    0    users_id_seq    SEQUENCE SET     <   SELECT pg_catalog.setval('public.users_id_seq', 150, true);
          public          postgres    false    216            W           2606    24593    users Users_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.users
    ADD CONSTRAINT "Users_pkey" PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.users DROP CONSTRAINT "Users_pkey";
       public            postgres    false    202            e           2606    57377 .   events_notifications events_notifications_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public.events_notifications
    ADD CONSTRAINT events_notifications_pkey PRIMARY KEY (id);
 X   ALTER TABLE ONLY public.events_notifications DROP CONSTRAINT events_notifications_pkey;
       public            postgres    false    213            c           2606    57379    events events_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.events DROP CONSTRAINT events_pkey;
       public            postgres    false    211            [           2606    57381 "   friend_requests friend_requests_pk 
   CONSTRAINT     v   ALTER TABLE ONLY public.friend_requests
    ADD CONSTRAINT friend_requests_pk PRIMARY KEY (user_from_id, user_to_id);
 L   ALTER TABLE ONLY public.friend_requests DROP CONSTRAINT friend_requests_pk;
       public            postgres    false    204    204            Y           2606    24601    friendships friendships_pkey 
   CONSTRAINT     j   ALTER TABLE ONLY public.friendships
    ADD CONSTRAINT friendships_pkey PRIMARY KEY (user1_id, user2_id);
 F   ALTER TABLE ONLY public.friendships DROP CONSTRAINT friendships_pkey;
       public            postgres    false    203    203            _           2606    32836    groups groups_pk 
   CONSTRAINT     N   ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_pk PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.groups DROP CONSTRAINT groups_pk;
       public            postgres    false    208            a           2606    57383    groups_users groups_users_pkey 
   CONSTRAINT     \   ALTER TABLE ONLY public.groups_users
    ADD CONSTRAINT groups_users_pkey PRIMARY KEY (id);
 H   ALTER TABLE ONLY public.groups_users DROP CONSTRAINT groups_users_pkey;
       public            postgres    false    210            ]           2606    32791    messages messages_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.messages DROP CONSTRAINT messages_pkey;
       public            postgres    false    206            t           2606    57389    events events_group_id_fkey    FK CONSTRAINT     |   ALTER TABLE ONLY public.events
    ADD CONSTRAINT events_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.groups(id);
 E   ALTER TABLE ONLY public.events DROP CONSTRAINT events_group_id_fkey;
       public          postgres    false    208    211    2911            u           2606    57394 7   events_notifications events_notifications_event_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.events_notifications
    ADD CONSTRAINT events_notifications_event_id_fkey FOREIGN KEY (event_id) REFERENCES public.events(id);
 a   ALTER TABLE ONLY public.events_notifications DROP CONSTRAINT events_notifications_event_id_fkey;
       public          postgres    false    211    213    2915            v           2606    57399 6   events_notifications events_notifications_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.events_notifications
    ADD CONSTRAINT events_notifications_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);
 `   ALTER TABLE ONLY public.events_notifications DROP CONSTRAINT events_notifications_user_id_fkey;
       public          postgres    false    213    202    2903            f           2606    57404 "   friendships friendships_users1__fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.friendships
    ADD CONSTRAINT friendships_users1__fk FOREIGN KEY (user2_id) REFERENCES public.users(id);
 L   ALTER TABLE ONLY public.friendships DROP CONSTRAINT friendships_users1__fk;
       public          postgres    false    2903    202    203            g           2606    57409 !   friendships friendships_users__fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.friendships
    ADD CONSTRAINT friendships_users__fk FOREIGN KEY (user1_id) REFERENCES public.users(id);
 K   ALTER TABLE ONLY public.friendships DROP CONSTRAINT friendships_users__fk;
       public          postgres    false    202    203    2903            n           2606    32807    groups groups_last_message_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_last_message_fkey FOREIGN KEY (last_message) REFERENCES public.messages(id);
 I   ALTER TABLE ONLY public.groups DROP CONSTRAINT groups_last_message_fkey;
       public          postgres    false    2909    206    208            o           2606    57414    groups groups_messages__fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.groups
    ADD CONSTRAINT groups_messages__fk FOREIGN KEY (last_message) REFERENCES public.messages(id);
 D   ALTER TABLE ONLY public.groups DROP CONSTRAINT groups_messages__fk;
       public          postgres    false    2909    206    208            q           2606    32847 '   groups_users groups_users_group_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.groups_users
    ADD CONSTRAINT groups_users_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.groups(id);
 Q   ALTER TABLE ONLY public.groups_users DROP CONSTRAINT groups_users_group_id_fkey;
       public          postgres    false    2911    210    208            r           2606    57419 $   groups_users groups_users_groups__fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.groups_users
    ADD CONSTRAINT groups_users_groups__fk FOREIGN KEY (group_id) REFERENCES public.groups(id);
 N   ALTER TABLE ONLY public.groups_users DROP CONSTRAINT groups_users_groups__fk;
       public          postgres    false    2911    208    210            p           2606    32842 &   groups_users groups_users_user_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.groups_users
    ADD CONSTRAINT groups_users_user_id_fkey FOREIGN KEY (user_id) REFERENCES public.users(id);
 P   ALTER TABLE ONLY public.groups_users DROP CONSTRAINT groups_users_user_id_fkey;
       public          postgres    false    210    2903    202            s           2606    57424 #   groups_users groups_users_users__fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.groups_users
    ADD CONSTRAINT groups_users_users__fk FOREIGN KEY (user_id) REFERENCES public.users(id);
 M   ALTER TABLE ONLY public.groups_users DROP CONSTRAINT groups_users_users__fk;
       public          postgres    false    210    2903    202            j           2606    32871    messages messages_group_id_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_group_id_fkey FOREIGN KEY (group_id) REFERENCES public.groups(id);
 I   ALTER TABLE ONLY public.messages DROP CONSTRAINT messages_group_id_fkey;
       public          postgres    false    2911    206    208            k           2606    57429    messages messages_groups1__fk    FK CONSTRAINT     ~   ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_groups1__fk FOREIGN KEY (user_from) REFERENCES public.users(id);
 G   ALTER TABLE ONLY public.messages DROP CONSTRAINT messages_groups1__fk;
       public          postgres    false    206    2903    202            l           2606    57434    messages messages_groups2__fk    FK CONSTRAINT     ~   ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_groups2__fk FOREIGN KEY (group_id) REFERENCES public.groups(id);
 G   ALTER TABLE ONLY public.messages DROP CONSTRAINT messages_groups2__fk;
       public          postgres    false    2911    208    206            m           2606    57439    messages messages_messages__fk    FK CONSTRAINT     �   ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_messages__fk FOREIGN KEY (reply_to) REFERENCES public.messages(id);
 H   ALTER TABLE ONLY public.messages DROP CONSTRAINT messages_messages__fk;
       public          postgres    false    206    206    2909            h           2606    32792    messages messages_reply_to_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_reply_to_fkey FOREIGN KEY (reply_to) REFERENCES public.messages(id);
 I   ALTER TABLE ONLY public.messages DROP CONSTRAINT messages_reply_to_fkey;
       public          postgres    false    206    2909    206            i           2606    32797     messages messages_user_from_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.messages
    ADD CONSTRAINT messages_user_from_fkey FOREIGN KEY (user_from) REFERENCES public.users(id);
 J   ALTER TABLE ONLY public.messages DROP CONSTRAINT messages_user_from_fkey;
       public          postgres    false    202    2903    206               �  x��VMoG=ӿ�NI 	��c{s�e)��vG���L�,��{�d�S �s7|��?a���/�W=�(/� �3=�U�^ի���?Z�\QzZe?z������o�D��B�'�����j9��[%��ҜK����eL+�F�RI�x�qV^S+�>���or�(S:U���ʂ-�ƺ|�5��:��Z0k���e�����xx>t���"^���e�rI����}z�����o�;�(Q�����g�[��g!�R�ԧ^#��8v�G��9��I���Q�.JS�|���F�_��>�S#N�Q�=Q�J��!fj�B'����UA8��*��O$I� ��WDؗ���r#�k����_�j���g{���V�� !q[)R�������0�B=+����X�|dߖpҍm�+^�����t�-M�ѽ�lqf>��z��h1[�'��dv�f�ֿ;�F�!v%6!G�Ap�I��g�"�|ɔ#�z�N���"�2�p��ccU|S�	_b[9Z��4�󹱔X�~17�Hh��^���af1�?$�"��\h�bT�)9a!�O �ûoK#Q�<Q9�l��s�I� �&PC��2^xlGXU�T���!/��1�U�w�qL�}�0�RD�0_��S����s*�~���)����V��7����lI�E����+�Jc����"��:8����^���6l%=6��M�uA�!C'�ٰo5�_[ Q2��~�|[�' �V9#`�� /�,kA<x��R�J�O������F�1%4��]R�WDwB���.!�zf	�Yi��hrzi�"F*/�ڄ�Gw��q��,�M_/����P�7E>ȅ�z���=t�E�py��c:�/�@訂t̃��Vr�&�уAhw�/�=Z�v� ���gɿD�щU&��IY�yi�l�P�aU �m����G�u[5x�f9��xe�RY^��R9��q`n8P���8}y���-�+�
�ß�gc��z��˦���qd��@�V��CK�~�z���	p|3��¡�epU��ȗ�Y�XN!��w���[�	�,䂅�^�$�1a���Y�;�������ؑ<?˺ӕ)���@��&0a��K�o?������b6�}�v҄�M���[�T��h�U�P�UD�����L?���7��F0�@���'���no��������pQCPv��qY�<�յ�����T[�5��}/.gw
Ӈ����FB\o�n�������%�lX�ݤ&^�"Ǘ-h���5�l��&Nl�;YL��-H���JlK�Mߗ�#��0�n P &[U�QU_s�������F�ps�9���~~r!	W:Ao>G�ܦMؑ���"��zg����%:��U��� ���tr��X�QD�Z��`��Z_o.�K��)բm��$�7rX��)ѱ��a�3ڡmo��Kh ]�ü��}��ˎ�NH�H?b b7��Q҆v�궬��v�;�~��,�wL	=>�W5y=�&I:h�k��P=�D�P�
"=�0N��YWp�!z�U���-�ZK���%\]���y��t��N��_	�f���+	q��3uuVL��ǖ�O��5�5�7+�����7;���.굋�B<'A���-��*�;l���4o*��K��!���<�q�1�t���I��
����x�Ј5���ND`�zw�Uur��O_����w��w�z^a�E.�f;>�	(���0��YT����Opúȫ�;w���g         0   x���  ��gOc�q�9|8X
5a��&���G�ܮ ��;���t      �   �   x�m��!E�o�"���=Ԓ��d�&��}��l܄�@� �mX��f������e�!�J��JƑѣ�%A�`PhT���FRwKLP[��^Q���#��Ha��:�aʂ����Яp&̐ך�s�[�%���\k8/D?�%fe�A��'�G�      �   B   x�Uɱ�0�X�E������=r ���n��l.;�d���i��k�l�̝�D~�{�� *4          9   x��I�0��4��;�+$���qX�t|ca����7��aK5�b�6#]�9?�.<�	�         y   x�%�� C��0=�t��?G���-��X��JƦ�6-)�e)���ǭ:Qm[���x.�C���ъ&�]ı.̓�(�=B�1/�K*8�F&#E6����<}����� p      �   ,  x�u��n9E��W0��`Y|��0��fô��%�#����R���[�x�e�x�͖���3�&�q�5���W�~�����߳���
J��w����e�8x����֥��҅�X�6���X���${��N�aKpU��{4u>vI2)Q}�?J?�`�-��I4�Xr�Y�eп�A��8�븼������Yo�������z\u?�W�݌�uz��Գa��Ye5��D��2蹴
T�)������d8V�k�}}��t�ǉe1	������6U��N�:eA{�u(si��F.'�$aU�I/S���Jmg����'c��
�:��޼���}��ԟ�R�qY<cK�!r�,u�&�-V坅H6Xƅ� �lVԿ�T��8շ�����kK�i�p��O�K�����{��P&��˗��>�	�8�*z�Εjş&�o=�����K�[;���1�Y0�P}8�hD�>Rd�m��ϱ�!fG0�>���.�R��%�����j7T����M�X�+�^�ɤ�r�KՊ;�u^�ᑷ�D��Y��u�z�˪�m�C��6����L�[���.�w&�1WΪ2�2�7��W,KV�kH����eQ�U�P�	U���s��HMB�:���|VȃFz��F$�VE���K��wo<2֚�>޿V�mc�Bn�.����ͤ�g�GC�l�2�6����bؠ	)6!%ߜ��;̛�M��xDK����w�`1�|+DԂL5�4����Pf�p�0g��)���C.0�%L9�mN8��>��ﳎy��`�e�،��F1�x�!y@c}˻��~3OOO �-��      �   g  x�mYM��8=k~���DG�v}��� ��,AC��^dYe�lKF��p��})��.�ٴJ%e�|�Rp�N7�'��5�ԓوA��?��~�-a�������m�?}o؋\|.NZ���ʵ���}���^���\F����E�uΗ�0c�]n�f_t���k���榢��x�-{�Miz���fB6���}�}�8U�����>�Q���se�i郌{��W�t{�\q�M㮚�r�e'\a]������>�Jc�����9����v�#Vx�>��K��9ڃPV��%|Þ$����McTe�2����oGQ|��g8{,�,[�ǦQ�[&��H�����^'�ׅ�F��&�#;]�䢗��x�"�Ri��7��y�J�t����v�1cod���ޙf���0u��B�Y�l���oٳ����S~�P��E��	�ו;�o�ٷ1�N,�=	��kU�I��?��=.Oq������/�R{t��]\y`�M�ٛJҙ�U�S�ܼ��*�5�G��!� �kUIƬ���L6zc%�I�T�ў��A>�����k\�)�Ac�erj�4��J ��{Gi�����5@6v��]oŒ���B[���}#�}���WK�1��<�Cg���X��ƕ˞[�� O����̷�����.J�!�K\�=��
�U����͒�dϾ����!���/��$�A�������������շ��7�S��c�<����N�o����|t�`m����7v|����w�D#�_���tGfy3�
e�,L؟ ��=��l ���y�.wW��O٣/����i�/ �ٻ�kz�@��'m��6DUm59?Q����e���,Qʓ�E���vrF*ot	^�|bǞ+96#n��!\���":`���^aK[ꇓ��ڳ�Ң���`��`�0{|#������2р→R����p.�oǇ�4q�
�=�}!L���H�����z�j{�7nd�xx�)J�׺��`�e5goO��5�kOYQr�yF��Pƣd�4�	z�k���s�ߊ���E6�l��\��i-߉Ҽ��p�2����}=�zdeN��r�d���C�d��
��'V��򃨆���G��0���[y1�@����=~��n���e{�I�q	{B9.E��2��ҩ�2dY��7�7��Y��υ�(i0��Sّ}r��	��O˚<Z���W�m7�� laW*(��&�c��T~#Z�׵�h �f:_�Z�n�|)�m���d�s��L�h�IH��6EQ��NXu&�Kј���Y����j�Y�(u��al�e�e1�Y�/ �7H�@�d`���F\un]�v�c�?�K�������\��Ԟ��g2R?y-�F�˪�m)T�n���\3�n.o���6�}��C�hh!Ͼ;ۅ�NoQQ˭v�s�H��Z�v*f�[q����^�~ހ+�*r�/�!C`�JK�7�_19�A�a�K��R �Ǎ �! 6'��r��]�f���r�k���m�7^ځ+��V��1��P�k��v�g��p�YI�ҩ�h��i��I�@� �ЏQ�>L�`#}M��Cr$z1���1�ʒ�4(��G˶�Ŋ�Hi�r!VT�ǯ���t����} (R�!Q����/�'���+T��s�r"��������X�	�����W���~��)K���2����=��4��5*��LN�A�B6�~)�}h�ة���5E�l����n �U7��R�+�� �vr�D(�
:���z�{�����B��h�T��n�c�ٿ=:�k���v�$C0 4d��tQ}:�,$�BA&ي��f�����hpؓ!�d'�H��f)�Æ0L1��gUM&ً	\q�����F֝�39PQ��`�.x �/I�'�C��$ǿ��C:1�#,{�lYE�a�%�7��b�<�젇H�>9[ִd���J������~�/������K��i|���b�>d�d79I�A�b����H{�x\�����@�,8�@&�v�i"8&eQKP�P#����$.��K��)��0�Ƃp��KʿI��&`����s59	��&t/�;"c�s$:�b�a�er#<��ɲ:A�h���4 �ig/9��Ўhd�(I����h�Z�c�.�p1]7u1��Ҭ�''i�B�v=��+HYSrkP>J6i���\�_�Ý.�0�q��mbE;��.F|y��&	2��0����n�̥֎1Qn^O��P��/W�<��!a�x�+�i���K~ �VBʮ���]E"{oQl2�eY��ċ�C��~$�$�i���14h���K�A$zS�@7����O��Bm�:ؤ4�c�=$��;|��&<�� ��dE?9� �_�7������%�\����L�7[�h�+�cH�OP��3���!���9��wP��SiY��u�6�������'�)**(���%#�k5�����D.�Fj�/��EfP�s��J�8�������I7��̸�w` ((f�:��m�E��Y�l�GΩLA�������*LN�H�\�N�-B#+C����br�TT>�><E�%��Ҥ���榙�x���3t!q�1J�4�!��/oP���CS���)b����;���)�sm���wT�@bl�W9{�nKC~3��Y	r��0�Q�z;��0UD�D�(��7☉��F����������dM�(Wz��i|���=<ـ@�odaq�VE����=F%��C�/�t0-����*�y��=5�Dj��g�!�饣	77-��sź:e��B�z�F�dR-Bc IzGX�;ɨQ��ߝ+�����zsӓ(� -xSC<٢K�i�&���K¤������Q��Г��]��|r�
7� ����9{�77Ĭ�����]4.���B��ŋF�4�i���/ޒxB���o�L]�mf/�E��:z�n��[!�_ZK`��6m~�T��H��#i�ٙt�N1~����}*ӄ�Ֆ����.\\''�P�eXy�^W�+���T����:���A���b�lɤ�%́�
��[�Ԇ����S����h�fX�� }v%=;�qҳa����8 n]�U�(�v��,&'Bԥ�xA��<�
)�Nnڋ���>��G8V<{���e��p�5��-���f|@�	*D�Ȧ�_=�f�}��h���ѸjvӫШ�>�w*�g	��
��&���O�Ӭ#�����;���|��t�
�^�z�.�h��,JR�� {�j���/�J{��x�	��1��sO&ۈ�^�S���̓Ɓ�;��9ڌ��y�f�@�f5��!�;9Y��c<;Ѓ�-������$�g/K�ݨY1;�G\j�}�HK����d=}�h�z�-�P�)�O�$�R6�f�_d������ ���     